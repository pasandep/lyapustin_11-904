package itis.Homework6;
import java.util.Scanner;

public class Homework6Task1{
	
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int k1 = 0; // переменная для сохранения наибольшей последовательности
		int k2 = 1; // переменная для сохранения текущей последовательности
		
		int b = sc.nextInt();
		
		while (b != 0){
			int a = b;
			b = sc.nextInt();
			if (b > a) {
				k2++;
				if (k2 >= k1) k1 = k2;
			} else k2 = 1;
		}
		System.out.print("Answer: " + k1);
	}
}
