package itis.Homework6;

public class Homework6Task2{
	
    public static void main(String[] args) {
		
		double n = 1;
		double a = 0;
		double b = (n)/(2*n*n*n+4*n*n+n+5); // сумма для n = 1
		double r = b - a;
		double sum = b;
		while (r > 0.000001) {
			n++;
			a = b;
			b = (n)/(2*n*n*n+4*n*n+n+5); // новая сумма 
			sum = sum + b;
			r = a - b;
		}
		System.out.print("Answer: " + sum);
	}	
}