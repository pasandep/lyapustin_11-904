import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class PaymentServiceTest {

    @Test
    public void testPaymentService1() throws NegativeAmountException { // общий
        Payment[] payments = new Payment[4];

        LocalDate date1 = LocalDate.of(2001, 11,13);
        payments[0] = new Payment(date1, 4200, "wrongResult");

        LocalDate date2 = LocalDate.of(2023, 12,19);
        payments[1] = new Payment(date2, 5400, "wrongResult");

        LocalDate date3 = LocalDate.of(2020, 5,19);
        payments[2] = new Payment(date3, 5800, "Пропитанный злостью и никотином");

        LocalDate date4 = LocalDate.of(2020, 5,15);
        payments[3] = new Payment(date4, 400, "wrongResult");

        Assertions.assertEquals("Пропитанный злостью и никотином", PaymentService.find(payments));
    }

    @Test
    public void testPaymentService2() throws NegativeAmountException { // одинаковая разница в датах, разная сумма
        Payment[] payments = new Payment[2];

        LocalDate date1 = LocalDate.of(2020, 5,16);
        payments[0] = new Payment(date1, 4200, "wrongResult");

        LocalDate date2 = LocalDate.of(2020, 5,20);
        payments[1] = new Payment(date2, 5400, "rightResult");

        Assertions.assertEquals("rightResult", PaymentService.find(payments));
    }

    @Test
    public void testPaymentService3() throws NegativeAmountException { // одинаковая разница в датах, одинаковая сумма
        Payment[] payments = new Payment[2];

        LocalDate date1 = LocalDate.of(2020, 5,16);
        payments[0] = new Payment(date1, 4200, "rightResult");

        LocalDate date2 = LocalDate.of(2020, 5,20);
        payments[1] = new Payment(date2, 4200, "wrongResult");

        Assertions.assertEquals("rightResult", PaymentService.find(payments));
    }

    @Test
    public void testPaymentService4() throws NegativeAmountException { // один платёж
        Payment[] payments = new Payment[1];

        LocalDate date1 = LocalDate.of(2020, 5,16);
        payments[0] = new Payment(date1, 4200, "rightResult");

        Assertions.assertEquals("rightResult", PaymentService.find(payments));
    }

    @Test
    public void testPaymentService5() throws NegativeAmountException { // одинаковая сумма, разная разница в дата
        Payment[] payments = new Payment[2];

        LocalDate date1 = LocalDate.of(2020, 5,17);
        payments[0] = new Payment(date1, 4200, "rightResult");

        LocalDate date2 = LocalDate.of(2020, 5,20);
        payments[1] = new Payment(date2, 4200, "wrongResult");

        Assertions.assertEquals("rightResult", PaymentService.find(payments));
    }

    @Test
    public void testPaymentService6() throws NegativeAmountException { // одинаковая сумма, разная разница в датах
        Payment[] payments = new Payment[2];

        LocalDate date1 = LocalDate.of(2020, 5,16);
        payments[0] = new Payment(date1, 4200, "wrongResult");

        LocalDate date2 = LocalDate.of(2020, 5,19);
        payments[1] = new Payment(date2, 4200, "rightResult");

        Assertions.assertEquals("rightResult", PaymentService.find(payments));
    }
}