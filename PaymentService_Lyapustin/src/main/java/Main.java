import java.time.LocalDate;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws NegativeAmountException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите количество платежей: ");
        int n = scanner.nextInt();
        Payment[] payments = new Payment[n];
        for (int i = 0; i < n; i++){
            System.out.println(i + 1 + ":");
            System.out.print("day: "); int day = scanner.nextInt();
            System.out.print("month: "); int month = scanner.nextInt();
            System.out.print("year: "); int year = scanner.nextInt();
            System.out.print("amount: "); int amount = scanner.nextInt();
            scanner.nextLine();
            System.out.print("name: "); String name = scanner.nextLine();
            LocalDate paymentDate = LocalDate.of(year, month, day);
            payments[i] = new Payment(paymentDate, amount, name);
        }
        System.out.println(PaymentService.find(payments));
    }
}
