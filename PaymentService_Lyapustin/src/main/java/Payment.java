import lombok.Getter;

import java.time.LocalDate;

@Getter
public class Payment {
    private LocalDate paymentDate;
    private long amount;
    private String name;

    public Payment(LocalDate paymentDate, long amount, String name) throws NegativeAmountException {
        if (paymentDate == null || name == null){
            throw new NullPointerException();
        }
        if (amount < 0){
            throw new NegativeAmountException();
        }
        this.paymentDate = paymentDate;
        this.amount = amount;
        this.name = name;
    }
}
