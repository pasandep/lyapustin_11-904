import lombok.Getter;

import java.time.LocalDate;

public class PaymentService {

    public static String find (Payment[] payments){
        Payment result = payments[0];
        for (int i = 1; i < payments.length; i++){
            if(nearestDateComparator(payments[i], result) == 1 || nearestDateComparator(result, payments[i]) == 0){
                result = payments[i];
            }
        }
        return result.getName();
    }

    private static int nearestDateComparator(Payment payment1, Payment payment2){
        MyDate now = new MyDate(LocalDate.now().toString());
        MyDate myDate1 = new MyDate(payment1.getPaymentDate().toString());
        MyDate myDate2 = new MyDate(payment2.getPaymentDate().toString());

        if (Math.abs(myDate1.getYear() - now.getYear()) < Math.abs(myDate2.getYear() - now.getYear())) {
            return 1;
        } else if ((Math.abs(myDate1.getYear() - now.getYear()) > Math.abs(myDate2.getYear() - now.getYear()))) {
            return -1;
        } else {
            if (Math.abs(myDate1.getMonth() - now.getMonth()) < Math.abs(myDate2.getMonth() - now.getMonth())) {
                return 1;
            } else if ((Math.abs(myDate1.getMonth() - now.getMonth()) > Math.abs(myDate2.getMonth() - now.getMonth()))) {
                return -1;
            } else {
                if (Math.abs(myDate1.getDay() - now.getDay()) < Math.abs(myDate2.getDay() - now.getDay())) {
                    return 1;
                } else if ((Math.abs(myDate1.getDay() - now.getDay()) > Math.abs(myDate2.getDay() - now.getDay()))) {
                    return -1;
                } else {
                    if (payment1.getAmount() > payment2.getAmount()) {
                        return 1;
                    } else if (payment1.getAmount() < payment2.getAmount()) {
                        return -1;
                    } else {
                        return earlierPaymentComparator(payment1, payment2);
                    }
                }
            }
        }
    }

    private static int earlierPaymentComparator(Payment payment1, Payment payment2){
        MyDate date1 = new MyDate(payment1.getPaymentDate().toString());
        MyDate date2 = new MyDate(payment2.getPaymentDate().toString());
        if (date1.getYear() < date2.getYear()){
            return 1;
        } else if(date1.getYear() > date2.getYear()){
            return -1;
        } else {

            if (date1.getMonth() < date2.getMonth()){
                return 1;
            } else if(date1.getMonth() > date2.getMonth()){
                return -1;
            } else {
                if (date1.getMonth() < date2.getMonth()){
                    return 1;
                } else if(date1.getMonth() > date2.getMonth()){
                    return -1;
                } else {

                    if (date1.getDay() < date2.getDay()){
                        return 1;
                    } else if(date1.getDay() > date2.getDay()){
                        return -1;
                    } else {
                        return 0;
                    }

                }
            }

        }
    }
}

@Getter
class MyDate{
    int year;
    int month;
    int day;

    public MyDate(String string){
        year = Integer.parseInt(string.split("-")[0]);
        month = Integer.parseInt(string.split("-")[1]);
        day = Integer.parseInt(string.split("-")[2]);
    }
}

