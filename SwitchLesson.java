import java.util.Scanner;

public class SwitchLesson{
	
    public static void main(String[] array) {
        Scanner sc = new Scanner(System.in);
		System.out.print("Choose one of S-ball, S-square, V-ball: ");
        String s = sc.nextLine();
        System.out.print("Enter r: ");
        double r = sc.nextDouble();
		if (r > 0) {
			switch (s) {
				
				case "S-ball": {
					System.out.print("S ball = " + (Math.PI*r*r));
					break;
				}
				
				case "S-square": {
					System.out.print("S square = " + (r*r));
					break;
				}
				
				case "V-ball": {
					System.out.print("V ball = " + ((4/3)*Math.PI*r*r*r));
					break;
				}
				
				default: {
					System.out.print("Err");
					break;
				} 
			}
		} else System.out.print("Err");
	}
}