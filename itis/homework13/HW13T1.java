package itis.homework13;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HW13T1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        Pattern FirstType = Pattern.compile("^(0[1-9]|1[1-2])/(0[1-9]|[12][0-9]|3[01])/([0-9]{4})\\s(0[1-9]|1[0-9]|2[0-3]):(0[1-9]|[1-5][0-9])");
        Pattern SecondType = Pattern.compile("^(0[1-9]|1[1-2])/(0[1-9]|[12][0-9]|3[01])/([0-9]{2})\\s(0[1-9]|1[0-9]|2[0-3]):(0[1-9]|[1-5][0-9])");
        Pattern ThirdType = Pattern.compile("^(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)/(0[1-9]|[12][0-9]|3[01])/([0-9]{0,4})\\s(0[1-9]|1[0-9]|2[0-3]):(0[1-9]|[1-5][0-9])");
        Matcher match1 = FirstType.matcher(s);
        Matcher match2 = SecondType.matcher(s);
        Matcher match3 = ThirdType.matcher(s);
        if (match1.find()){                           // Если нужно было выводить true или false, нужно лишь заменить эти if на один if со знаком ИЛИ
            System.out.println("MM/DD/YYYY HH:MM");
        } else if (match2.find()){
            System.out.println("MM/DD/YY HH:MM");
        } else if (match3.find()){
            System.out.println("MMM/DD/YYYY HH:MM");
        } else {
            System.out.println("Err! No matches with pattenrs!");
        }
    }
}
