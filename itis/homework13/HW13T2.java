package itis.homework13;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HW13T2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        Pattern mail = Pattern.compile("[A-Za-z0-9]+[\\.]*+[A-Za-z0-9]+[@][a-zA-Z0-9]+[\\.]*+[a-zA-Z]+[\\.][a-z]{2,4}");
        Matcher matcher = mail.matcher(s);
        while (matcher.find()){
            System.out.println(s.substring(matcher.start(), matcher.end()));
        }
    }
}
