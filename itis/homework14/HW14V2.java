package itis.homework14;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HW14V2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter line: ");
        String s = sc.nextLine();
        Pattern name = Pattern.compile("\\b[^.!?][A-ZА-Я][a-zа-яё]+\\b");
        Matcher m = name.matcher(s);
        System.out.print("Answer:");
        while (m.find()){
            System.out.print(s.substring(m.start(), m.end()) + ";");
        }
    }
}
