package itis.homework14;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HW14V1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter line: ");
        String s = sc.nextLine();
        Pattern name = Pattern.compile("\\b[^.!?][A-ZА-ЯЁ][a-zа-яё]+\\b");
        Matcher m = name.matcher(s);
        StringBuilder answer = new StringBuilder();
        while(m.find()){
            answer.append(s.substring(m.start(),m.end())).append(";");
        }
        System.out.println("Answer:" + answer);
    }
}
