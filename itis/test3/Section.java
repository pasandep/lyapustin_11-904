package itis.test3;

public class Section {
    protected Point point1 = new Point();
    protected Point point2 = new Point();

    public Section(int x1, int y1, int x2, int y2){
        point1.setX(x1);
        point1.setY(y1);
        point2.setX(x2);
        point2.setY(y2);
    }

    public class Point{
        private int x;
        private int y;
        public Point(){
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
    }

    public double length(){
        double d;
        d = (double) Math.sqrt((point2.getX() - point1.getX())*(point2.getX() - point1.getX())+(point2.getY() - point1.getY())*(point2.getY() - point1.getY()));
        return d;
    }
}
