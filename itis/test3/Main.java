package itis.test3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Через пробел/enter введите значения вершин отрезка (x1, y1, x2, y2):");
        Section section = new Section(scanner.nextInt(),scanner.nextInt(),scanner.nextInt(),scanner.nextInt());
        System.out.println("Длина отрезка равна " + section.length());
        System.out.println("Через пробел/enter введите название фигуры, а затем значения вершин треугольника (name, x1, y1 , x2, y2, x3, y3):");
        RightTriangle rightTriangle = new RightTriangle(scanner.nextInt(),scanner.nextInt(),scanner.nextInt(),scanner.nextInt(),scanner.nextInt(),scanner.nextInt());
        System.out.println(rightTriangle.acreage());
        rightTriangle.print();
    }
}

