package itis.test3;

import itis.test3.Section;

enum Color {RED, YELLOW, GREEN}

public class RightTriangle extends Figure implements Printable {
    private Color color;
    private Section section1;
    private Section section2;
    private Section section3;

    public RightTriangle(int x1, int y1, int x2, int y2, int x3, int y3){
        this.section1 = new Section(x1, y1, x2, y2);
        this.section2 = new Section(x2, y2, x3, y3);
        this.section3 = new Section(x3, y3, x1, x1);
    }

    private double mostLengthEdge(){
        if (section1.length() >= section2.length()){
            if (section1.length() >= section3.length()){
                return section1.length();
            } else {
                return section3.length();
            }
        } else {
            if (section2.length() >= section3.length()){
                return section2.length();
            } else {
                return section3.length();
            }
        }
    }

    public double acreage() {
        return ((section1.length()*section2.length()*section3.length())/(2.0*mostLengthEdge()));
    }

    private void setColorOfFigure(){
        String name;
        if ((section1.length() + section2.length() + section3.length()) > 100){
            color = Color.RED;
        } else if ((section1.length() + section2.length() + section3.length())> 20){
            color = Color.YELLOW;
        } else {
            color = Color.GREEN;
        }
    }

    private int perimetr(){
        return (int) (section3.length() + section2.length() + section1.length());
    }

    private Color getColor() {
        setColorOfFigure();
        return color;
    }

    public void print() {
        System.out.println("Figure: Right Triangle ");
        System.out.println("Perimetr: " + perimetr());
        System.out.println("Color: " + getColor());
    }
}
