package itis.homework9;
import java.util.Scanner;
import java.util.Random;

public class Hw9T1 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Random r = new Random();

        System.out.print("Enter n: ");
        int n = sc.nextInt();
        int[] a = new int[n];
        int max = -1;
        int sum = 0;
        int k = 0;

        for (int i = 0; i < n; i++){
            a[i] = r.nextInt(n);
            System.out.print(a[i] + " ");
            if (a[i] > max){
                max = a[i];
                k = i % 2 != 0? 1 : 0;
            } else if ((a[i] == max) && (i % 2 != 0)){
                k++;
            }
            sum = i % 2 != 0? sum + a[i] : sum;
        }
        sum = sum - k*max;
        System.out.println();
        System.out.println("k = " + k);
        System.out.println("Max: " + max);
        System.out.print("Answer: " + sum);
    }
}
