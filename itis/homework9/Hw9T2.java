package itis.homework9;
import java.util.Scanner;
import java.util.Random;

public class Hw9T2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Random r = new Random();

        System.out.print("Enter n: ");
        int n = sc.nextInt();
        int[] a = new int[n];
        int min = n;
        int k = 0;

        for (int i = 0; i < n; i++){
            a[i] = r.nextInt(n);
            System.out.print(a[i] + " ");
            min = a[i] < min? a[i] : min;
        }

        for (int i = 0; i < n; i ++){
            k = a[i] == min ? k + 1 : k;
        }

        System.out.println();
        System.out.println("Min: " + min);
        System.out.print("Answer: " + k);
    }
}
