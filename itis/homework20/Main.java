package itis.homework20;

import java.util.Scanner;
import java.util.function.BinaryOperator;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите n, m и нужно ли заполнить матрицу случайными числами(boolean): ");
        Matrix matrix1 = new Matrix(scanner.nextInt(),scanner.nextInt(), scanner.nextBoolean());
        matrix1.print();
        System.out.println("Введите n, m и нужно ли заполнить матрицу случайными числами(boolean): ");
        Matrix matrix2 = new Matrix(scanner.nextInt(), scanner.nextInt(),scanner.nextBoolean());
        matrix2.print();
        BinaryOperator<Matrix> f1 = (bmatrix1, bmatrix2) -> {
            bmatrix1 = matrix1;
            bmatrix2 = matrix2;
            if (bmatrix1.getM() == bmatrix2.getN()) {
                    Matrix matrixRes = new Matrix(bmatrix1.getN(), bmatrix2.getM(), false);
                int number = 0;
                    for (int i = 0; i < bmatrix1.getN(); i++){
                        for (int j = 0; j < bmatrix2.getM(); j++){
                            for (int k = 0; k < bmatrix1.getN(); k++){
                                number += bmatrix1.getSpecificValues(i,k) * bmatrix2.getSpecificValues(k,j);
                                matrixRes.setValues(i, j, number);
                            }
                            number = 0;
                        }
                    }
                    return matrixRes;
            } else {
                System.out.println("Err!");
                return null;
            }
        };        Matrix matrix = f1.apply(matrix1, matrix2);
        System.out.println("Result: ");
        matrix.print();
    }
}

