package itis.homework20;

import java.util.Random;

public class Matrix {
    private int[][] values;
    private int n;
    private int m;


    public Matrix(int n, int m, boolean fillIn){
        this.n = n;
        this.m = m;
        int[][] a = new int[n][m];
            Random random = new Random();
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (fillIn){
                        a[i][j] = random.nextInt(10);
                } else {
                        a[i][j] = 0;
                    }
            }
            values = a;
        }
    }

    public int getN(){
        return n;
    }

    public int getM(){
        return m;
    }

    public int[][] getValue(){
        return values;
    }

    public int getSpecificValues(int a, int b) {
        return values[a][b];
    }

    public void setValues(int a, int b, int number){
        values[a][b] = number;
    }

    public void print(){
        for (int i = 0; i < n; i++){
            for (int j = 0; j < m; j++){
                System.out.print(values[i][j] + " ");
            }
            System.out.println();
        }
    }
}
