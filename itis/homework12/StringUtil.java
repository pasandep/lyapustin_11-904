package itis.homework12;

public class StringUtil {
    public static int compareTo(String s1, String s2){
        int n = Math.min(s1.length(), s2.length());
        String[] strs = new String[2];
        for (int i = 0; i < n; i++){
            if (s1.charAt(i) > s2.charAt(i)){
                return 1;
            } else if(s1.charAt(i) < s2.charAt(i)) {
                return -1;
            }
        }
        return 0;
    }
}
