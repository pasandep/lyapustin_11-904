package itis.homework12;

import java.util.Scanner;

public class HW12T1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter n: ");
        int n = sc.nextInt();
        sc.nextLine();
        if (n > 0) {
            String[] strings = new String[n];
            for (int i = 0; i < strings.length; i++) {
                strings[i] = sc.nextLine();
            }

            String a = "";
            for (int i = 0; i < n - 1; i++) {
                for (int j = 0; j < n - 1; j++) {
                    int k = StringUtil.compareTo(strings[j], strings[j + 1]);
                    if (k == 1) {
                        a = strings[j];
                        strings[j] = strings[j + 1];
                        strings[j + 1] = a;
                    }
                }
            }
            for (int i = 0; i < n; i++) {
                System.out.println(i + 1 + ":" + strings[i]);
            }
        }  else {
            System.out.println("Err! n must be positive!");
        }
    }
}
