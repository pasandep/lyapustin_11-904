package itis.homework11;

import java.util.Scanner;

public class HW11T2Dop {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter n: ");
        int n = sc.nextInt();
        if (n > 0) {
            int[][] a = new int[n][n];
            for (int i = 0; i < n; i++){
                System.out.print(" ");
            }
            a[0][0] = 1;
            if (a[0][0] % 2 == 0){ // По сути вместо этого if'a можно просто вывести 0, но вы подобную строку сказали заменить
                System.out.println("*");
            } else {
                System.out.println("0");
            }
            for (int i = 1; i < n; i++){
                for (int k = 0; k < n-i; k++){
                    System.out.print(" ");
                }
                a[i][0] = 1;
                System.out.print("0 ");
                for (int j = 1; j < n; j++){
                    a[i][j] = a[i-1][j] + a[i-1][j-1];
                    if (a[i][j] != 0) {
                        if (a[i][j] % 2 == 0) {
                            System.out.print("* ");
                        } else {
                            System.out.print("0 ");
                        }
                    }
                }
                System.out.println();
            }
        } else {
            System.out.println("Err! n must be positive!");
        }
    }
}