package itis.homework11;

import java.util.Random;
import java.util.Scanner;

public class  HW11T1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        System.out.print("Enter n: ");
        int n = sc.nextInt();
        if (n > 0) {
            System.out.print("Enter m: ");
            int m = sc.nextInt();
            if (m > 0) {

                int[][] a = new int[n][m + 1];

                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        a[i][j] = r.nextInt(m);
                        a[i][m] += a[i][j];
                        System.out.print(a[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                for (int j = 0; j < n - 1; j++) {
                    for (int i = 0; i < n - 1; i++) {
                        if (a[i][m] > a[i + 1][m]) {
                            int[] b = a[i];
                            a[i] = a[i + 1];
                            a[i + 1] = b;
                        }
                    }
                }
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        System.out.print(a[i][j] + " ");
                    }
                    System.out.println("| Sum: " + a[i][m]);
                }
            } else {
                System.out.println("Err! m must be positive!");
            }
        } else {
            System.out.println("Err! n must be positive!");
        }
    }
}
