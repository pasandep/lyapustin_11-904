package itis.SecondSem.Homework1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException{
        Scanner fileScanner = new Scanner(new File("itis\\SecondSem\\Homework1\\input.txt"));
        String s;
        do {
            try{
                System.out.println(TableParser.parseData(fileScanner.nextLine()));
            } catch (IncorrectUserDataException incorrectUserDataException){
                incorrectUserDataException.printStackTrace();
                incorrectUserDataException.getMessage();
            } catch (IncorrectYearException incorrectYearException){
                incorrectYearException.printStackTrace();
                incorrectYearException.getMessage();
            }
        } while (fileScanner.hasNextLine());
    }
}
