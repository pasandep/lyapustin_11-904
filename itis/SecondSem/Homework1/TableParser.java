package itis.SecondSem.Homework1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TableParser {

    public static int parseData(String string) throws IncorrectYearException, IncorrectUserDataException {
        Pattern dataPattern = Pattern.compile("^([A-Za-z]*[0-9]*)+\\s*;\\s*[0-9]+\\s*;$");
        Matcher dataMatcher = dataPattern.matcher(string);
        Pattern yearPattern = Pattern.compile(";\\s*[0-9]+\\s*;");
        Matcher yearMatcher = yearPattern.matcher(string);
        if (dataMatcher.find()){
            yearMatcher.find();
            int year = Integer.parseInt(string.substring(yearMatcher.start() + 1, yearMatcher.end() - 1).trim());
            if ((year > 0) && (year < 2021)){
                return 2020-year;
            } else {
                throw new IncorrectYearException();
            }
        } else {
            throw new IncorrectUserDataException();
        }
    }
}