package itis.homework19;

public interface Limb {
    void wash();
    void cutNails();
    void playWithPaws();
}
