package itis.homework19;

import java.util.Scanner;

enum Action {VOICE, BRINGWAND, ASKFORAFFECTION, RUN, EAT,  SLEEP}

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Dog dog = new Dog("Amir");
        dog.dogLimb.wash();
        dog.dogLimb.cutNails();
        dog.dogLimb.playWithPaws();
        Dog.DogLocality.setLocality("Ufa");
        System.out.println("Dog location: " + Dog.DogLocality.getLocality());
        System.out.print("Enter how much actions you want to do: ");
        int i = scanner.nextInt();
        for (int j = 0; j < i; j++) {
            dog.action();
        }
    }
}
