package itis.homework19;

import java.util.Objects;
import java.util.Scanner;

public class Dog extends Animal {

    private int countofwands = 0;
    private String nickname;
    public DogLimb dogLimb = new DogLimb();

    public Dog(String nickname) {
        super();
        this.nickname = nickname;
    }

    public static class DogLocality extends Locality{
        private static String locality;

        public static String getLocality() {
            return locality;
        }

        public static void setLocality(String locality1) {
            locality = locality1;
        }
    }

    protected class DogLimb implements Limb {

        public void wash() {
            System.out.println("Paws was washed!");
            System.out.println("-Gav tyav!(Now my paws are clean!)");
            System.out.println();
        }

        public void cutNails() {
            System.out.println("Nails was cutted!!");
            System.out.println("-Woof!(So good with cropped nails =))");
            System.out.println();
        }

        public void playWithPaws() {
            System.out.println("Playing with doggy!");
            System.out.println("-Woof gav!(My owner is the best in the world!)");
            System.out.println();
        }
    }

    public void action(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Choose action: 1.VOICE, 2.RUN, 3.EAT, 4.SLEEP, 5.BRINGWAND, 6.ASKFORAFFECTION (enter number only): ");
        int n = scanner.nextInt();
        String name = "";
        switch (n) {
            case 1:
                name = "VOICE";
                break;
            case 2:
                name = "RUN";
                break;
            case 3:
                name = "EAT";
                break;
            case 4:
                name = "SLEEP";
                break;
            case 5:
                name = "BRINGWAND";
                break;
            case 6:
                name = "ASKFORAFFECTION";
                break;
            default:
                System.out.println("Err: not action!");
        }
        Action action = Action.valueOf(name);
        switch (action) {
            case EAT:
                eat("meat");
                break;
            case SLEEP:
                sleep();
                break;
            case RUN:
                run();
                break;
            case VOICE:
                voice();
                break;
            case ASKFORAFFECTION:
                askForAffection();
                break;
            case BRINGWAND:
                bringWand();
                break;
        }
    }

    public void voice() {
        System.out.println("Hell... khm,GAV!");
        System.out.println();
    }

    public void bringWand(){
        if (energy > 2){
            int chance = random.nextInt(101);
            if (chance <= 55) {
                System.out.println("Good job! Wand is here!");
                countofwands++;
            } else {
                System.out.println("Nice try, but the wand got lost somewhere along the way =(");
            }
            energy -= 3;
            System.out.println("Energy: " + energy);
        } else {
            System.out.println("Not enough energy! Sleep or eat something!");
        }
        System.out.println();
    }

    public int getCountofwands() {
        return countofwands;
    }

    public void askForAffection(){
        int chance = random.nextInt(101);
        if (chance <= 30){
            System.out.println("Someone from your human friends noticed you and stroked you!");
        } else {
            System.out.println("Now you're out of luck, friend, all the people are busy =(");
        }
        System.out.println();
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return countofwands == dog.countofwands &&
                Objects.equals(nickname, dog.nickname);
    }

    public int hashCode() {
        return Objects.hash(countofwands, nickname);
    }

    public String toString() {
        return "Dog{" +
                "countofwands=" + countofwands +
                ", nickname='" + nickname + '\'' + '}';
    }
}