package itis.term2.homework5;

import itis.exam.Array;

import java.util.Iterator;

public class ASMain {
    public static void main(String[] args) {
        ArrayStack<String> arrayStack = new ArrayStack(7);
        arrayStack.push("12");
        arrayStack.push("13");
        arrayStack.push("207");
        arrayStack.push("7");
        arrayStack.push("109");
        System.out.println(arrayStack.top());
        arrayStack.push("25");
        arrayStack.push("37");
        arrayStack.push("210");
        arrayStack.push("109");
        arrayStack.push("97");
        Iterator iterator = arrayStack.iterator();
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        arrayStack.printAll();
        System.out.println(arrayStack.pop());
        System.out.println(arrayStack.top());
        System.out.println(arrayStack.top());
        Iterator iterator2 = arrayStack.iterator();
        while (iterator2.hasNext()){
            System.out.print(iterator2.next() + " ");
        }
        System.out.println();
        arrayStack.printAll();
        Iterator iterator1 = arrayStack.iterator();
    }
}
