package itis.term2.homework5;

public interface ArrayCollection<T> {
    // добавление значение value в конец коллекции, остальные элементы не меняются
    void add(T value);

    // добавление значение value на место index
    // если добавление прошло успешно - возвращает true
    // если index < 0 или > arr.length - emptySize - возвращает false
    // перезатирает старое значение
    boolean add(int index, T value);

    // возвращает значение элемента под номером index
    // если такого нет - null
    T get(int index);

    // возвращает номер элемента (первое вхождение) со значением value
    // -1 если не элемент не найден
    int indexOff(T value);

    // возвращает количество заполненных ячеек колллекции
    int size();

    // возвращает элемент под номером index, сдвигает коллекцию влево
    T remove(int index);
}

