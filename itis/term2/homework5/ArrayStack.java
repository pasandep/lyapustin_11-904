package itis.term2.homework5;

import java.util.Arrays;
import java.util.Iterator;

public class ArrayStack<T> implements Iterable<T> {
    private T[] array;
    private int emptySize;
    private int quantity;

    public ArrayStack(int length){
        array = (T[]) new Object[length];
        emptySize = length;
        quantity = 0;
    }

    private class ArrayStackIterator implements Iterator<T>{
        int currentIndex = 0;

        @Override
        public boolean hasNext() {
            return (currentIndex < array.length - emptySize);
        }

        @Override
        public T next() {
            if (hasNext()){
                currentIndex++;
                return array[currentIndex - 1];
            }
            return null;
        }
    }

    public void push(T value){
        if (emptySize != 0) {
            array[array.length - emptySize] = value;
            quantity++;
            emptySize--;
        } else {
            emptySize = array.length;
            array = Arrays.copyOf(array, array.length*2+1);
            array[array.length - emptySize - 1] = value;
            quantity++;
        }
    }

    public Object top(){
        if (isEmpty()){
            System.out.println("Array is empty!");
            return null;
        } else {
            return array[quantity - 1];
        }
    }

    public Object pop(){
        if (isEmpty()){
            System.out.println("Array is empty");
            return null;
        } else {
            Object saver = array[quantity - 1];
            array[quantity - 1] = null;
            emptySize++;
            quantity--;
            return saver;
        }
    }

    public void printAll(){
        if (isEmpty()){
            System.out.println("Array is empty");
        } else {
            for (T value : array) {
                if (value != null) {
                    System.out.print(value + " ");
                }
            }
            System.out.println();
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayStackIterator();
    }

    public boolean isEmpty(){
        return quantity == 0;
    }
}
