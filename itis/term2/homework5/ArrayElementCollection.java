package itis.term2.homework5;

import java.util.Arrays;
import java.util.Iterator;

public class ArrayElementCollection<T> implements ArrayCollection<T>, Iterable<T> {
    private T[] arr;
    private int emptySize;

    ArrayElementCollection(int length){
        arr = (T[]) new Object[length];
        emptySize = length;
    }

    @Override
    public void add(T value){
        if (emptySize > 0){
            arr[arr.length - emptySize] = value;
            emptySize--;
        } else {
            emptySize = arr.length ;
            arr = Arrays.copyOf(arr, arr.length*2 + 1);
            arr[arr.length-emptySize - 1] = value;
        }
    }

    @Override
    public boolean add(int index, T value) {
        if(index >= 0 && index <= arr.length - emptySize){
                arr[index] = value;
                return true;
        } else {
            return false;
        }
    }

    public T get(int index){
        return index >= 0 && index < arr.length - emptySize ? arr[index] : null;
    }

    @Override
    public int indexOff(T value) {
        for (int i = 0; i < arr.length - emptySize; i++) {
            if (arr[i] == value) return i;
        }
        return -1;
    }

    @Override
    public int size() {
        return arr.length-emptySize;
    }

    @Override
    public T remove(int index) {
        if(index >= 0 && index < arr.length - emptySize) {
            T saver = arr[index];
            for (int i = index; i < arr.length - emptySize - 1; i++) {
                arr[i] = arr[i + 1];
            }
            arr[arr.length - emptySize - 1] = null;
            emptySize++;
            return saver;
        } else {
            return null;
        }
    }

    private class  ArrayElementCollectionIterator implements Iterator<T>{
        int currentIndex = 0;

        @Override
        public boolean hasNext() {
            return currentIndex < arr.length - emptySize;
        }

        public T next(){
            if (hasNext()){
                currentIndex++;
                return arr[currentIndex - 1];
            }
            return null;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayElementCollectionIterator();
    }
}