package itis.term2.homework5;

import java.util.Iterator;

public class AELMain {
    public static void main(String[] args) {
        ArrayElementCollection arrayElementCollection = new ArrayElementCollection(2);
        arrayElementCollection.add("one");
        System.out.println(arrayElementCollection.get(0));
        System.out.println(arrayElementCollection.indexOff("one"));
        arrayElementCollection.add("two");
        arrayElementCollection.add("three");
        Iterator iterator = arrayElementCollection.iterator();
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        System.out.println(arrayElementCollection.get(2));
        System.out.println(arrayElementCollection.indexOff("two"));
        arrayElementCollection.add("five");
        arrayElementCollection.add(3, "four");
        arrayElementCollection.add("six");
        arrayElementCollection.add("Nikita");
        arrayElementCollection.add("Alba");
        Iterator iterator2 = arrayElementCollection.iterator();
        while (iterator2.hasNext()){
            System.out.print(iterator2.next() + " ");
        }
        System.out.println();
        arrayElementCollection.remove(0);
        arrayElementCollection.remove(arrayElementCollection.size()-1);
        arrayElementCollection.remove(2);
        Iterator iterator3 = arrayElementCollection.iterator();
        while (iterator3.hasNext()){
            System.out.print(iterator3.next() + " ");
        }
        arrayElementCollection.remove(4);
    }
}
