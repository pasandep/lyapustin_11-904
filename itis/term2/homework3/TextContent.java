package itis.term2.homework3;

public class TextContent {
        private String title;
        private String text;

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public TextContent(String title, String text){
        this.title = title.trim();
        this.text = text.trim();
    }
}
