package itis.term2.homework3;

import org.w3c.dom.Text;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Введите заголовок объявления (не более 30 символов): ");
            String title = scanner.nextLine();
            System.out.print("Введите текст объявления (не более 100 символов): ");
            String text = scanner.nextLine();
            System.out.print("Введите дату посылки в формате (dd-MM-yyyy HH:mm): ");
            String date = scanner.nextLine();
            Add<TextContent> add = new Add<TextContent>(title, text, date);
            if (TextAddValidator.validator(add)) {
                System.out.println("Add is correct!");
            } else {
                System.out.println("Add is incorrect!");
            }
        } catch (DateTimeException dateException){
            System.err.println(dateException.getMessage());
        }
    }
}
