package itis.term2.homework3;

import java.time.LocalDateTime;

public class TextAddValidator<T> {

    public static boolean validator(Add<? extends TextContent> add) {
        if ((add.getContent().getTitle().length() > 0 ) && (add.getContent().getText().length() <= 30)){
            if ((add.getContent().getText().length() > 0) && (add.getContent().getText().length() <= 100)){
                LocalDateTime now = LocalDateTime.now();
                if (add.getStartDateTime().isAfter(now)){
                    return false;
                }
                if (add.getStartDateTime().isBefore(now.minusDays(30))){
                    return false;
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
