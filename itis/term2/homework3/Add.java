package itis.term2.homework3;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Add<T> {
    private T content;

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    private LocalDateTime startDateTime;

    public T getContent() {
        return content;
    }

    public Add(String title, String text, String date){
        content = (T) new TextContent(title, text);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        startDateTime = LocalDateTime.parse(date, formatter);
    }

}