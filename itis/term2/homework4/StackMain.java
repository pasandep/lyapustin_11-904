package itis.term2.homework4;

import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StackMain {
    public static void main(String[] args) {
        int count = 0;
        Stack<Double> stack = new Stack<>();
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        StringTokenizer stringTokenizer = new StringTokenizer(string, ",");
        double answer = 0;
        while(stringTokenizer.hasMoreTokens()){
            String token = stringTokenizer.nextToken().trim();
            if (    token.length() == 1 && (
                    token.charAt(0) == '*'
                    || token.charAt(0) == '-'
                    || token.charAt(0) == '/'
                    || token.charAt(0) == '+')) {
                answer = count == 0? stack.calculate(stack.pop(), stack.pop(), token) : stack.calculate(answer, stack.pop(), token);
                count++;
            } else {
                stack.push(Double.parseDouble(token));
            }
        }
        System.out.println(answer);
    }
}
//-29, 13, 2, 7, 20, 19, 28, -, *, +, /, -, -

