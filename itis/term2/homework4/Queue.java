package itis.term2.homework4;

public class Queue<T extends Integer> {
    private int size;
    private Element<Integer> back;
    private Element<Integer> front;

    private static class Element<T>{
        private T value;
        private Element<T> next;

        public T getValue(){
            return value;
        }

        Element(T value){
            this.value = value;
        }
    }

    public void push(Integer value){
        Element<Integer> newEl = new Element<>(value);
        if (front == null){
            front = newEl;
        } else {
            newEl.next = back;
        }
        back = newEl;
        size++;
    }



    public void printAll(){
        Queue<Integer> soutQueue = getReversedCopy();
        Element<Integer> element = soutQueue.back;
        while (element.next != null){
            System.out.print(element.value + " ");
            element = element.next;
        }
        System.out.println(element.value);
    }

    public int pop(){
        if (size != 0) {
            if (size == 1) {
                var x = front.value;
                front = null;
                size--;
                return x;
            } else {
                var saver = front.value;
                Element<Integer> el = back;
                while (el.next.next != null) {
                    el = el.next;
                }
                front = el;
                front.next = null;
                size--;
                return saver;
            }
        } else {
            System.err.println("Err!");
            return 0;
        }
    }


    public Queue<Integer> sort(){
        Element<Integer> element = back;
        java.lang.Integer[] a = new java.lang.Integer[size];
        for (int i = 0; i < size; i++){
            a[i] = (int) element.value;
            element = element.next;
        }

        for (int i = 0; i < a.length; i++){
            for (int j = 1; j < a.length; j++){
                if (a[j] > a[j-1]){
                    int x = a[j];
                    a[j] = a[j-1];
                    a[j-1] = x;
                }
            }
        }
        Queue<Integer> sortedQueue = new Queue<>();
        for (int i = a.length-1; i >= 0; i--){
            sortedQueue.push(a[i]);
        }
        return sortedQueue;
    }

    public static Queue<Integer> concatAndSort(Queue<Integer> queue1, Queue<Integer> queue2){
        Queue<Integer> queueResult = new Queue<>();
        Element<Integer> element = queue1.back;
        while(element.next != null){
            queueResult.push(element.value);
            element = element.next;
        }
        queueResult.push(element.value);
        element = queue2.back;
        while(element.next != null){
            queueResult.push(element.value);
            element = element.next;
        }
        queueResult.push(element.value);
        queueResult =  queueResult.sort();
        return queueResult;
    }

    public int getSize(){
        return size;
    }

    private Queue<Integer> getReversedCopy(){
        Queue<Integer> copy = new Queue<>();
        Element<Integer> element = this.back;
        while(element.next != null){
            copy.push(element.value);
            element = element.next;
        }
        copy.push(element.value);
        return copy;
    }
}
