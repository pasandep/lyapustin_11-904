package itis.term2.homework4;

public class Stack<T> {
    private class Element<T>{
        private T value;
        private Element<T> next;

        Element(T value){
            this.value = value;
        }
    }
    private int size;
    private Element<T> topElement;

    public void push(T newValue){
        Element<T> newElement = new Element<>(newValue);
        newElement.next = topElement;
        topElement = newElement;
        size++;
    }

    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return topElement == null;
    }

    public T top(){
        return topElement != null? topElement.value : null;
    }

    public void printAll(){
        Element<T> newElement = topElement;
        while(newElement!= null){
            System.out.print(newElement.value + " ");
            newElement = newElement.next;
        }
        System.out.println();
    }

    public T pop(){
        Element<T> topSaver = topElement;
        topElement = topElement.next;
        size--;
        return topSaver.value;
    }

    public double calculate(double firstValue, double secondValue, String mathAction) {
        switch (mathAction.charAt(0)) {
            case '*':
                return secondValue * firstValue;
            case '/':
                return secondValue / firstValue;
            case '+':
                return secondValue + firstValue;
            case '-':
                return secondValue - firstValue;
            default:
                return 0;
        }
    }
}