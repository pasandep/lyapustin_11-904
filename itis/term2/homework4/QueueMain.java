package itis.term2.homework4;

public class QueueMain {
    public static void main(String[] args) {
        Queue<Integer> queue1 = new Queue<>();
        queue1.push(3);
        queue1.pop();
        queue1.printAll();
        System.out.println();

        queue1.push(18);
        queue1.push(12);
        queue1.push(102);
        queue1.push(19);
        queue1.printAll();

        queue1 = queue1.sort();
        queue1.printAll();
        System.out.println();

        Queue<Integer> queue2 = new Queue<>();
        queue2.push(192);
        queue2.push(4);
        queue2.push(1);
        queue2.push(32);
        queue2.push(6);
        queue2.printAll();

        queue2 = queue2.sort();
        queue2.printAll();
        System.out.println();

        Queue<Integer> queue3 = Queue.concatAndSort(queue1, queue2);
        queue3.printAll();
    }
}