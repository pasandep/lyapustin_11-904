package itis.sem2.hw6;

public class Student {
    private int averageScore;
    private int year;
    private String fio;

    public Student(String FIO, int year, int averageScore){
        fio = FIO;
        this.year = year;
        this.averageScore = averageScore;
    }

    public void printInfo(){
        if (this != null) {
            System.out.println(fio + " | year: " + year + " | average score: " + averageScore);
        } else {
            System.out.println("null student");
        }
    }

    public static void printForAll(Student[] students){
        for (int i = 0; i < students.length; i++){
            if (students[i] != null) {
                students[i].printInfo();
            } else {
                System.out.println("null");
            }
        }
    }

    public int getAverageScore() {
        return averageScore;
    }

    public int getYear() {
        return year;
    }

    public String getFio() {
        return fio;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
