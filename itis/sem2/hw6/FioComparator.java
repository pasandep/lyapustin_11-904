package itis.sem2.hw6;

import java.util.Comparator;

public class FioComparator<T extends Student> implements Comparator<T> {

    @Override
    public int compare(T student1, T student2) {
        if (student1 != null && student2 != null && student1.getFio() != null && student2.getFio() != null) {
            return student1.getFio().compareToIgnoreCase(student2.getFio());
        }
        return -1;
    }
}
