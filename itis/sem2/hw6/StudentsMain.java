package itis.sem2.hw6;

import java.util.Comparator;

public class StudentsMain {
    public static void main(String[] args) {
        Student[] students = new Student[7];
        students[0] = new Student("Lyapustin Nikita Sergeevich", 2001, 99);
        students[1] = new Student("Ховаев Михаил Михалыч", 2002, 67);
        students[2] = new Student("Akhmadiev Albert Maratovich", 1987, 63);
        students[3] = new Student("Sosyuk Nikita Mikhailovich", 2015, 27);
        students[4] = new Student("Потребчук Олег", 2015, 27);
        students[5] = new Student("Иванов Иван Иванович", 2005, 57);
        students[6] = new Student("Latypov Rishat Ildarovich", 1555, 78);
        Student.printForAll(students);
        System.out.println();
        FioComparator<Student> fioComparator = new FioComparator<>();
        Comparator<Student> yearComparator = (Student student1, Student student2) -> {
            if (student1 != null && student2 != null){
                if (student1.getYear() > student2.getYear()) return 1;
                if (student1.getYear() < student2.getYear()) return -1;
                return 0;
            } return -1;
        };
        Comparator<Student> averageScoreComparator = new Comparator<Student>() {
            @Override
            public int compare(Student student1, Student student2) {
                if (student1 != null && student2 != null){
                    if (student1.getAverageScore() > student2.getAverageScore()) {
                        return 1;
                    } else if (student1.getAverageScore() < student2.getAverageScore()){
                        return - 1;
                    } else {
                        return 0;
                    }
                } return -1;
            }
        };
        sort(students, fioComparator);
        System.out.println("by fio:");
        Student.printForAll(students);
        System.out.println();

        sort(students, averageScoreComparator);
        System.out.println("by averageScore:");
        Student.printForAll(students);
        System.out.println();

        sort(students, yearComparator);
        System.out.println("by year:");
        Student.printForAll(students);
        System.out.println();
    }

    public static <T> void sort(Student[] students,
                                        Comparator<Student> comparator){
        for (int i = 0; i < students.length - 1; i++){
            for (int j = i + 1; j < students.length; j++){
                if (comparator.compare(students[i], students[j]) >= 0){
                    Student saver = students[i];
                    students[i] = students[j];
                    students[j] = saver;
                }
            }
        }
    }
}
