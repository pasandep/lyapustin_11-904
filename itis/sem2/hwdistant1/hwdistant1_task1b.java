package itis.sem2.hwdistant1;

import java.util.ArrayList;
import java.util.Scanner;

public class hwdistant1_task1b {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] a = {1, 2, 3};

        ArrayList<ArrayList<Integer>> result = new ArrayList<>();

        result.add(new ArrayList<>());

        for (int value : a) {
            ArrayList<ArrayList<Integer>> current = new ArrayList<>();

            for (ArrayList<Integer> l : result) {
                int size = l.size() + 1;
                for (int j = 0; j < size; j++) {
                    l.add(j, value);
                    ArrayList<Integer> temp = new ArrayList<>(l);
                    current.add(temp);
                    l.remove(j);
                }
            }
            result = new ArrayList<>(current);
        }

        System.out.println();
        System.out.println("final result");
        for (ArrayList res : result) {
            System.out.println(res);
        }

        System.out.println();
        System.out.println("Task 1b:");
        System.out.println("Введите перестановки: ");
        StringBuilder s = new StringBuilder(scanner.nextLine());
        ArrayList<ArrayList<Integer>> task = input(s, a.length);
        if (task.size() == result.size()) {
            boolean answer = true;
            boolean[] checked = new boolean[result.size()]; // Для проверки того, была ли уже определённая перестановка
            for (int i = 0; i < task.size(); i++) {
                if (result.contains(task.get(i)) && !checked[result.indexOf(task.get(i))]){
                    checked[result.indexOf(task.get(i))] = true;
                } else {
                    answer = false;
                }
            }
            System.out.println(answer);
        } else {
            System.out.println(false);
        }
    }

    public static ArrayList<ArrayList<Integer>> input(StringBuilder string, int n){
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        string.deleteCharAt(0).deleteCharAt(string.length()-1);
        String array = string.substring(0, string.length()).replaceAll("], \\[", ", ");
        for (int i = 0; i < array.split(", ").length; i+=n){
            ArrayList<Integer> helper = new ArrayList<>();
            for (int j = i; j < i+n; j++){
                helper.add(Integer.parseInt(array.split(", ")[j]));
            }
            result.add(helper);
        }
        return result;
    }
}