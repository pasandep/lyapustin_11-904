package itis.sem2.test3.Annotations;

public enum LogLevel {
    DEBUG,
    INFO,
    ERROR
}
