package itis.sem2.test3;

import itis.sem2.test3.Exceptions.AnnotationNotFoundException;
import itis.sem2.test3.Exceptions.EmptyValueException;

import java.io.IOException;

public interface AnnotationProcessor {
    boolean process(final Class<?> cl, final Object object) throws EmptyValueException, AnnotationNotFoundException, IOException, IllegalAccessException;
}
