package itis.sem2.test3;

import itis.sem2.test3.Annotations.LogFile;
import itis.sem2.test3.Annotations.LogLevel;
import itis.sem2.test3.Annotations.LogValue;
import itis.sem2.test3.Exceptions.AnnotationNotFoundException;
import itis.sem2.test3.Exceptions.EmptyValueException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AnnotationProcessorTest {
    private AnnotationProcessor processor;
    @BeforeEach
    public void prepare() {
        processor = new AnnotationProcessorImpl();
    }
    @Test
    public void testSuccess_defaultLevelTrue() throws Exception {
        var cat = new Cat1();
        cat.setName("Murzik");
        var result = processor.process(Cat1.class, cat);
        assertTrue(result);
    }
    @LogFile(path = "./itis/sem2/test3/var2/input1.txt")
    class Cat1 {
        private String ownerName;
        @LogValue
        private String name;
        public void setName(String name) {
            this.name = name;
        }
    }
    @Test
    public void testSuccess_debugLevelTrue() throws Exception {
        var cat = new Cat2();
        cat.setAge(2);
        var result = processor.process(Cat2.class, cat);
        assertTrue(result);
    }

    @Test
    public void testSuccess_False() throws Exception {
        var cat = new Cat1();
        assertThrows(EmptyValueException.class, () ->
                processor.process(Cat1.class, cat));
    }

    @LogFile(path = "./itis/sem2/test3/var2/input2.txt")
    class Cat2 {
        private String name;
        @LogValue(level = LogLevel.DEBUG)
        private int age;
        public void setAge(int age) {
            this.age = age;
        }
    }

    class Cat3 {
        private String ownerName;
        @LogValue
        private String name;
        public void setName(String name) {
            this.name = name;
        }
    }

    @Test
    public void testMyTestNegative() throws Exception {
        var cat = new Cat3();
        assertThrows(AnnotationNotFoundException.class, () ->
                processor.process(Cat3.class, cat));
    }

    @Test
    public void testMyTest2() throws Exception {
        var cat = new Cat3();
        assertThrows(AnnotationNotFoundException.class, () ->
                processor.process(Cat3.class, cat));
    }

    @LogFile(path = "./itis/sem2/test3/var2/input2.txt")
    class Cat4{
        @LogValue(level = LogLevel.DEBUG)
        private Home home;

        @LogValue(level = LogLevel.ERROR)
        private String name;

        @LogValue
        private int age;

        private String color;

        Cat4(String adress, String name, int age, String color){
            home = new Home();
            home.setAdress(adress);
            this.name = name;
            this.age = age;
            this.color = color;
        }
    }

    class Home{
        String adress;

        public void setAdress(String adress) {
            this.adress = adress;
        }

        @Override
        public String toString() {
            return adress;
        }
    }

    @Test
    public void testMyTest3() throws Exception {
        var cat = new Cat4("Ufa", "Barsik", 17, "orange");
        var result = processor.process(Cat4.class, cat);
        assertTrue(result);
    }
}

