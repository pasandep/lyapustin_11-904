package itis.sem2.test3;

import itis.sem2.test3.Annotations.LogFile;
import itis.sem2.test3.Annotations.LogLevel;
import itis.sem2.test3.Annotations.LogValue;
import itis.sem2.test3.Exceptions.AnnotationNotFoundException;
import itis.sem2.test3.Exceptions.EmptyValueException;

import java.io.*;
import java.lang.reflect.Field;

public class AnnotationProcessorImpl implements AnnotationProcessor{

    @Override
    public boolean process(Class<?> cl, Object object) throws EmptyValueException, AnnotationNotFoundException, IOException, IllegalAccessException {
        try (BufferedWriter log = new BufferedWriter(new FileWriter(new File(cl.getAnnotation(LogFile.class).path())))) {

            if (cl.getAnnotation(LogFile.class) == null) {
                throw new AnnotationNotFoundException();
            }

            int count = 0;
            Field[] fields = cl.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                field.setAccessible(true);
                if (field.isAnnotationPresent(LogValue.class)) {
                    if (field.get(object) != null) {

                        LogLevel logLevel = field.getAnnotation(LogValue.class).level();
                        String name = field.getName();
                        log.write(logLevel + " " + name + " = " + field.get(object).toString() + "\n");
                        log.flush();
                        count++;

                    } else {
                        throw new EmptyValueException();
                    }
                }
            }

            if(count > 0){
                return true;
            }

            return false;

        } catch (IllegalArgumentException | IOException e) {
            return false;
        }
    }
}
