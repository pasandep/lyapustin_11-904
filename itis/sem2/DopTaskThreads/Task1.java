package itis.sem2.DopTaskThreads;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Task1 {
    public static <Url> void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter URL: ");
        Document doc = Jsoup.parse(new URL(scanner.nextLine()), 500000);

        System.out.print("Enter format (with \".\"): ");
        String format = scanner.nextLine();

        Elements collections1 = doc.select("img[src$=" + format + "]");

        System.out.print("k = ");
        int k = scanner.nextInt();

        ExecutorService executorService = Executors.newFixedThreadPool(k);
        int count = 0;
        for(Element element : collections1) {
            int finalCount = count;
            executorService.submit(() -> {
                try {
                    String url = element.absUrl("src");
                    URL srcurl = new URL(url);
                    InputStream inputStream = srcurl.openStream();
                    Files.copy(inputStream, new File("./itis/sem2/DopTaskThreads/Imgs/" + "src" + Thread.currentThread().toString() + finalCount + format).toPath());
                } catch (IOException exc) {
                    exc.printStackTrace();
                }
            });
            count++;
        }
        executorService.shutdown();
    }
}

