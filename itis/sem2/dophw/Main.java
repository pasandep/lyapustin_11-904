package itis.sem2.dophw;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        Class userClass = Class.forName("itis.sem2.dophw.User");
        Field[] userFields = userClass.getFields();

        System.out.println("Task7: ");
        Map<Class, List<Field>> map = Arrays.stream(userFields).collect(Collectors.groupingBy(Field::getType));

        for (int i = 0; i < userFields.length; i++){
            if (map.get(userFields[i].getType()) != null) {
                System.out.print(userFields[i].getType() + ": ");
                System.out.println(map.get(userFields[i].getType()));
                map.put(userFields[i].getType(), null);
            }
        }
        /*for (Map.Entry<?, ?> m : map.entrySet()){
            System.out.println(m);
        }*/

        System.out.println("Task8: ");

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter full name of class: ");
        Class inputClass = Class.forName(scanner.nextLine());
        Object someClass = inputClass.newInstance();
        System.out.print("Enter n: "); int n = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < n; i++){
            try {
                String fieldInfo = scanner.nextLine().trim();
                String type = fieldInfo.split("\\s")[0].trim();
                String name = fieldInfo.split("\\s")[1].trim();
                String value = fieldInfo.split("\\s")[2].trim();
                Field field = inputClass.getDeclaredField(name);
                field.setAccessible(true);
                switch (type) {
                    case "byte":
                        field.setByte(someClass, Byte.parseByte(value));
                        break;
                    case "short":
                        field.setShort(someClass, Short.parseShort(value));
                        break;
                    case "int":
                        field.setInt(someClass, Integer.parseInt(value));
                        break;
                    case "long":
                        field.setLong(someClass, Long.parseLong(value));
                        break;
                    case "float":
                        field.setFloat(someClass, Float.parseFloat(value));
                        break;
                    case "double":
                        field.setDouble(someClass, Double.parseDouble(value));
                        break;
                    case "char":
                        field.setChar(someClass, value.charAt(0));
                        break;
                    case "boolean":
                        field.setBoolean(someClass, Boolean.parseBoolean(value));
                        break;
                    default:
                        field.set(someClass, value);
                }
            } catch (IllegalArgumentException exc){
                System.out.println("К сожалению, программа работает только для примитивных типов и String =(");
            }
        }
        Method print = inputClass.getDeclaredMethod("printAll");
        print.setAccessible(true);
        print.invoke(someClass);
    }
}

class User {
    private int years;
    public String username;
    public int id;
    public String firstName;
    public String secondName;

    public User() {

    }

    public User(String username, int id) {
        this.username = username;
        this.id = id;
    }

    public User(int years, String username, int id, String firstName, String secondName) {
        this.years = years;
        this.username = username;
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public void print() {
        System.out.println("id: " + id);
    }

    private void printAll() {
        print();
        System.out.println("username: " + username + "\nfirstname: " + firstName + "\nsecondName: " + secondName + "\nyears: " + years);
    }

    public String getUsername() {
        return username;
    }
}