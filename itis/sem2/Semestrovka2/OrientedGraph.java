package itis.sem2.Semestrovka2;

import java.util.Arrays;
import java.util.Scanner;

public class OrientedGraph {
    private int[][] matrix;
    private long iterationsCount;

    public OrientedGraph(int[][] matrix){
        this.matrix = matrix;
        iterationsCount = 0;
    }

    public OrientedGraph(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter size: ");
        int n = scanner.nextInt();
        matrix = new int[n][n];
        System.out.println("Enter path length (-1 if there are no way): ");
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                System.out.print((i + 1) + " -> " + (j + 1) + ": ");
                matrix[i][j] = scanner.nextInt();
            }
        }
        iterationsCount = 0;
    }

    public int[][] getShortestWay(){
        int[][] helper = new int[matrix.length][matrix.length];
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix.length; j++){
                if (i == j){
                    helper[i][j] = 0;
                } else {
                    helper[i][j] = matrix[i][j];
                }
            }
        }
        for (int k = 0; k < helper.length; k++){
            for (int i = 0; i < helper.length; i++){
                for (int j = 0; j < helper.length; j++){
                    if (helper[i][k] != -1 && helper[k][j] != -1) {
                        if (helper[i][j] != -1) {
                            helper[i][j] = Math.min(helper[i][j], helper[i][k] + helper[k][j]);
                        } else {
                            helper[i][j] = helper[i][k] + helper[k][j];
                        }
                    }
                    iterationsCount++;
                }
            }
        }
        return helper;
    }

    public void print(){
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix.length; j++){
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public long getIterationsCount() {
        long saver = iterationsCount;
        iterationsCount = 0;
        return saver;
    }
}
