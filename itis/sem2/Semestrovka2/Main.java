package itis.sem2.Semestrovka2;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        /*OrientedGraph orientedGraph = new OrientedGraph();
        int [][] matrix = orientedGraph.getShortestWay();
        orientedGraph.print();
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix.length; j++){
                System.out.print(matrix[i][j] + "   ");
            }
            System.out.println();
        }*/
        Random r = new Random();
        for (int k = 100; k <= 1000; k+=50){
            System.out.println("k = " + k);
            long time = 0;
            OrientedGraph orientedGraph = null;
            for(int n = 0; n < 10; n++) {       //Для нахождения среднего значения среди 10
                int[][] matrix = new int[k][k];
                for (int j = 0; j < k; j++) {
                    for (int i = 0; i < k; i++) {
                        matrix[i][j] = r.nextInt();
                    }
                }
                orientedGraph = new OrientedGraph(matrix);
                long start = System.nanoTime();
                int[][] answer = orientedGraph.getShortestWay();
                long end = System.nanoTime();
                time = time + (end - start);
            }
            System.out.println(k + ": " + "\n" + "time: " + (time/10) + "\n" + "iterations: " + orientedGraph.getIterationsCount() + "\n");
        }
    }
}