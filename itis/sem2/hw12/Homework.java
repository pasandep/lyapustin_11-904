package itis.sem2.hw12;

public class Homework {

    public static String task1(String string) throws EmptyStringException {
        try {
            if (string != null && !string.equals("")) {
                char[] array = string.toCharArray();
                int outputIndex = 0;

                int start = 0;
                while (array[start] == ' ') {
                    if (array[start] != ' ') {
                        start--;
                        break;
                    }
                    start++;
                    if (start + 1 == array.length) throw new EmptyStringException();
                }

                int end = array.length - 1;
                while (array[end] == ' ') {
                    end--;
                    if (array[end] != ' ') {
                        break;
                    }
                }

                char[] output = new char[end - start + 1];

                int wordLength = 0;
                boolean onlyOneSpace = true;

                for (int i = end; i >= start; i--) {
                    if (array[i] == ' ' || i == start) {
                        for (int j = i; j <= i + wordLength; j++) {
                            if (array[j] != ' ') {
                                output[outputIndex] = array[j];
                                outputIndex++;
                                onlyOneSpace = true;
                            }
                        }
                        if (i != start) {
                            if (onlyOneSpace) {
                                output[outputIndex] = ' ';
                                outputIndex++;
                                onlyOneSpace = false;
                            }
                        }
                        wordLength = 0;
                    }
                    wordLength++;
                }

                int outputEnd = output.length - 1;
                while (output[outputEnd] == '\u0000') {
                    outputEnd--;
                    if (output[outputEnd] != '\u0000') {
                        break;
                    }
                }

                char[] finalOutput = new char[outputEnd + 1];

                for (int i = 0; i <= outputEnd; i++) {
                    finalOutput[i] = output[i];
                }

                String answer = new String(finalOutput);
                return answer;
            }
            if (string == null) {
                return null;
            } else {
                return "";
            }
        } catch (EmptyStringException e){
            e.printStackTrace();
            return null;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////

    public static int task2(String string) throws EmptyStringException{
        try {
            if (string == null || string.equals("")) throw new EmptyStringException();

            char[] input = string.toCharArray();
            int inputArrayIndex = 0;

            while (input[inputArrayIndex] == ' ') {
                if (inputArrayIndex == input.length - 1) throw new EmptyStringException();
                inputArrayIndex++;
            }

            boolean negative = false;
            if (input[inputArrayIndex] == '-') {
                negative = true;
                inputArrayIndex++;
            } else {
                if (input[inputArrayIndex] == '+') {
                    inputArrayIndex++;
                }
            }

            int endOfInteger = inputArrayIndex;
            while (input[endOfInteger] != ' ' && endOfInteger < input.length - 1) {
                endOfInteger++;
                if (input[endOfInteger] == ' ') {
                    endOfInteger--;
                    break;
                }
            }

            int outputSize = endOfInteger - inputArrayIndex + 1;
            if (outputSize > 9) throw new OutOfIntegerSizeException();
            char[] output = new char[outputSize];

            for (int i = 0; i < outputSize; i++) {
                output[i] = input[inputArrayIndex];
                inputArrayIndex++;
            }

            int answer = 0;
            boolean rightInput = true;

            for (int i = 0; i < outputSize; i++) {
                if (Homework.checkOnNumber(output[i])) {
                    answer = answer + Homework.symbolToIntWithDigit(output[i], outputSize - i);
                } else {
                    rightInput = false;
                }
            }
            if (rightInput) {
                if (negative) {
                    return -answer;
                } else {
                    return answer;
                }
            } else {
                return 0;
            }
        } catch (EmptyStringException | OutOfIntegerSizeException ese){
            ese.printStackTrace();
            return 0;
        }
    }

    private static boolean checkOnNumber(char symbol){
        if (symbol == '0' ||
                symbol == '1' ||
                symbol == '2' ||
                symbol == '3' ||
                symbol == '4' ||
                symbol == '5' ||
                symbol == '6' ||
                symbol == '7' ||
                symbol == '8' ||
                symbol == '9'){
            return true;
        }
        return false;
    }

    private static int symbolToIntWithDigit(char symbol, int digitNumberFrom1){ // метод, получающий цифру и номер разряда, возвращающий число данного разряда
        int number = 0;                                                         // например, цифра 3, разряд 3(сотни), вернёт 300
        switch (symbol){
            case '0':
                number = 0;
                break;
            case '1':
                number = 1;
                break;
            case '2':
                number = 2;
                break;
            case '3':
                number = 3;
                break;
            case '4':
                number = 4;
                break;
            case '5':
                number = 5;
                break;
            case '6':
                number = 6;
                break;
            case '7':
                number = 7;
                break;
            case '8':
                number = 8;
                break;
            case '9':
                number = 9;
                break;
        }
        int digit = 1;
        for (int i = 1; i < digitNumberFrom1; i++){
            digit = digit * 10;
        }
        return number*digit;
    }
}
