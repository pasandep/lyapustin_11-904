package itis.sem2.hw12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;



public class HomeworkTest {
    @Test
    public void task1testEmptyString(){                       // пример задания
        String exceptedResult = null;
        String actualResult = Homework.task1("     ");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task1test0(){                       // пример задания
        String exceptedResult = null;
        String actualResult = Homework.task1(null);
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task1test1(){                       // пример задания
        String exceptedResult = "blue is sky the";
        String actualResult = Homework.task1("the sky is blue");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task1test2(){                       // больше слов
        String exceptedResult = "Lumen is one of my favorite rockbands";
        String actualResult = Homework.task1("rockbands favorite my of one is Lumen");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task1test3(){                       // пустая строка
        String exceptedResult = "";
        String actualResult = Homework.task1("");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task1test4(){                       // один символ
        String exceptedResult = "1";
        String actualResult = Homework.task1("1");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task1test5(){                       // произвольный набор различных символов
        String exceptedResult = "a b c d e f";
        String actualResult = Homework.task1("f e d c b a");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task1test6(){                       // неодносимвольное слово
        String exceptedResult = "ab";
        String actualResult = Homework.task1("ab");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task1test7(){                       // два единичных символа
        String exceptedResult = "a b";
        String actualResult = Homework.task1("b a");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task1test8(){                       // с пробелами
        String exceptedResult = "a b";
        String actualResult = Homework.task1(" b a ");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test1(){                       // общий1
        int exceptedResult = 203;
        int actualResult = Homework.task2("203");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test2(){                       // общий2
        int exceptedResult = 17;
        int actualResult = Homework.task2("17 всегда война");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test3(){                       // общий3
        int exceptedResult = 0;
        int actualResult = Homework.task2("всегда 17 всегда война");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test4(){                       // на числа с минусом
        int exceptedResult = -123;
        int actualResult = Homework.task2("-123 минус один два три");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test5(){                       // на один лишь минус
        int exceptedResult = 0;
        int actualResult = Homework.task2("- минус");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test6(){                       // Нецелочисленные
        int exceptedResult = 0;
        int actualResult = Homework.task2("1.123");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test7(){                       // Нецелочисленные отрцицательные
        int exceptedResult = 0;
        int actualResult = Homework.task2("-1.123");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test8(){                       // Число с буквой О
        int exceptedResult = 0;
        int actualResult = Homework.task2("1O2");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test9(){                       // Нули спереди
        int exceptedResult = 101;
        int actualResult = Homework.task2("00101");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test10(){                      // С плюсом в начале
        int exceptedResult = 101;
        int actualResult = Homework.task2("+101");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test11(){                      // Пустая строка
        Assertions.assertThrows(EmptyStringException.class, () ->{
            Homework.task2(null);
        });
    }

    @Test
    public void task2test12(){                      // Пустая строка
        int exceptedResult = 0;
        int actualResult = Homework.task2("");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test13(){                      // Вне границ int
        Assertions.assertThrows(OutOfIntegerSizeException.class, () ->{
            Homework.task2("9999999999");
        });
    }

    @Test
    public void task2test14(){                      // Вне границ int
        int exceptedResult = 0;
        int actualResult = Homework.task2("9999999999");
        Assertions.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void task2test15(){                      // Вне границ int
        int exceptedResult = 0;
        int actualResult = Homework.task2("-9999999999");
        Assertions.assertEquals(exceptedResult, actualResult);
    }
}
