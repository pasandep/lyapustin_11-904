package itis.sem2.test1;

import itis.sem2.Classwork4.Stack;

public class LinkedDeque<T> implements Deque<T>{
    private class Element<T>{
        private T value;
        private Element<T> next;
        private Element<T> previous;

        Element(T value){
            this.value = value;
        }
    }
    private Element<T> head;
    private Element<T> tail;
    private int size = 0;

    @Override
    public boolean addToHead(T value) {
        Element<T> newEl = new Element<>(value);
        if (head == null){
            head = newEl;
            tail = head;
        } else {
            newEl.next = head;
            head = newEl;
            newEl.next.previous = newEl;
        }
        size++;
        return true;
    }

    @Override
    public boolean addToTail(T value) {
        Element<T> newEl = new Element<>(value);
        if (tail == null){
            tail = newEl;
            head = tail;
        } else {
            newEl.previous = tail;
            tail = newEl;
            newEl.previous.next = newEl;
        }
        size++;
        return true;
    }

    public void printAll(){
        if (isEmpty()){
            System.out.println("Empty Deque!");
        } else {
            Element<T> element = head;
            while (element.next != null) {
                System.out.print(element.value + " ");
                element = element.next;
            }
            System.out.println(element.value + ";");
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T pollFromHead() throws NoElementException {
        if(head != null){
            if (size == 1){
                Element<T> saver = head;
                head = null;
                size--;
                return saver.value;
            } else {
                Element<T> saver = head;
                head = head.next;
                head.previous = null;
                size--;
                return saver.value;
            }
        } else {
            throw new NoElementException();
        }
    }

    @Override
    public T pollFromTail() throws NoElementException {
        if(head != null){
            if (size == 1){
                Element<T> saver = head;
                head = null;
                size--;
                return saver.value;
            }
            Element<T> saver = tail;
            tail = tail.previous;
            tail.next = null;
            size--;
            return saver.value;
        } else {
            throw new NoElementException();
        }
    }

    @Override
    public int compareTo(Deque o) {
        if (size > o.size()){
            return 1;
        } else if (size < o.size()){
            return -1;
        } else {
            return 0;
        }
    }
}
