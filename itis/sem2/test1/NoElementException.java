package itis.sem2.test1;

public class NoElementException extends RuntimeException {
    @Override
    public String getMessage() {
        return "The requested element does not exist";
    }
}
