package itis.sem2.test1;

public interface Deque<E> extends Comparable<Deque> {

    /**
     * Add element to the head of the Collection
     * returns true in case of success
     */
    boolean addToHead(E e);
    /**
     * Add element to the tail of the Collection
     * returns true in case of success
     */
    boolean addToTail(E e);
    int size();
    boolean isEmpty();
    /**
     * Retrieves and removes the head of this queue,
     * or throws NoElementException if this queue is empty.
     * NoElementException - unchecked exception
     */
    E pollFromHead() throws NoElementException;
    /**
     * Retrieves and removes the tail of this queue,
     * or throws NoElementException if this queue is empty.
     * NoElementException - unchecked exception
     */
    E pollFromTail() throws NoElementException;

}
