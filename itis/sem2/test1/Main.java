package itis.sem2.test1;

public class Main {
    public static void main(String[] args) throws NoElementException {
        LinkedDeque<String> linkedDeque1 = new LinkedDeque<>();
        linkedDeque1.addToHead("dumal");
        linkedDeque1.addToHead("Snachala");
        linkedDeque1.addToTail("tyazhelo");
        linkedDeque1.printAll();
        LinkedDeque<String> linkedDeque2 = new LinkedDeque<>();
        linkedDeque2.printAll();
        linkedDeque2.addToHead("okazalos");
        linkedDeque2.addToHead("A");
        linkedDeque2.addToTail("ochen");
        linkedDeque2.addToTail("tyazhelo");
        linkedDeque2.addToTail("blin");
        linkedDeque2.printAll();
        System.out.println(linkedDeque2.pollFromTail());
        linkedDeque2.printAll();
        System.out.println(linkedDeque2.pollFromHead());
        System.out.println(linkedDeque1.compareTo(linkedDeque2));
    }
}
