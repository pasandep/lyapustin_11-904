package itis.sem2.test2;

import itis.exam.Array;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.ls.LSOutput;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {
        Document doc = Jsoup.parse(new URL("https://habr.com/ru"), 500000);

        System.out.println("Task 1: ");
        Elements elementsLinks = doc.select("a[href]");
        Stream<Element> elementStream = elementsLinks.stream();
        System.out.println(elementStream
            .distinct()
            .count());
        String surl = elementsLinks.get(elementsLinks.size() - 1).absUrl("href");
        URL url = new URL(surl);
        System.out.println(surl);
        Files.copy(url.openStream(), new File("./test1task1.html").toPath());

        System.out.println("\n" + "Task2: ");
        ArrayList<String> h2attrs = new ArrayList<>();
        Elements elementsH2 = doc.getElementsByTag("h2");
        for (int i = 0; i < elementsH2.size(); i++){
            h2attrs.add(elementsH2.get(i).text());
        }
        Stream<String> stringStreamH2 = h2attrs.stream();
        stringStreamH2
                .filter(x -> x.length() <= 50)
                .sorted()
                .forEach(System.out::println);

        System.out.println("\n" + "Task3: ");
        ArrayList<String> h3attrs = new ArrayList<>();
        Elements elementsH3 = doc.getElementsByTag("h3");
        for (int i = 0; i < elementsH3.size(); i++){
            h3attrs.add(elementsH3.get(i).text());
        }
        Stream<String> stringStreamH3 = h3attrs.stream();
        System.out.println(stringStreamH3
                .mapToInt(x -> x.length())
                .max()
                .orElse(-1));
    }
}
