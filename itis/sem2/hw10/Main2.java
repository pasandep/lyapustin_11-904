package itis.sem2.hw10;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main2 {
    public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Scanner scanner = new Scanner(new File("./json"));
        try {
            String classNameString = scanner.nextLine();
            Pattern classNamePattern = Pattern.compile("\"[A-Za-z0-9]+\"");
            Matcher classNameMatcher = classNamePattern.matcher(classNameString);
            classNameMatcher.find();
            Class cl = Class.forName("itis.sem2.hw10." + classNameString.substring(classNameMatcher.start() + 1, classNameMatcher.end() - 1));
            Object result = cl.newInstance();
            Pattern fieldPattern = Pattern.compile("\"[A-Za-z0-9]+\":\\s\"([A-Za-z]+)|([0-9]+)\"");
            String fieldString = scanner.nextLine();
            Matcher fieldMatcher = fieldPattern.matcher(fieldString);
            while (scanner.hasNextLine()) {
                Field field = cl.getDeclaredField(fieldString.split("\"")[1]);
                field.setAccessible(true);
                if (field.getType().equals(int.class)) {
                    Positive positive = field.getAnnotation(Positive.class);
                    int integer = Integer.parseInt(fieldString.split("\"")[3]);
                    if (positive != null){
                        if (integer > 0){
                            field.set(result, integer);
                        } else {
                            throw new InvalidDataException();
                        }
                    } else {
                        field.set(result, integer);
                    }
                } else { //Вот тут можно было добавить проверку, на String
                    Length length = field.getAnnotation(Length.class);
                    String value = fieldString.split("\"")[3];
                    if (length != null){
                        if (value.length() > length.min() && value.length() < length.max()){
                            field.set(result, value);
                        } else {
                            throw new InvalidDataException();
                        }
                    } else {
                        field.set(result, value);
                    }
                }
                fieldString = scanner.nextLine();
                fieldMatcher = fieldPattern.matcher(fieldString);
            }
            Method print = cl.getDeclaredMethod("print");
            System.out.println(cl.getName().toString());
            print.setAccessible(true);
            print.invoke(result);
        } catch (ClassNotFoundException | NoSuchMethodException | NoSuchFieldException exc) {
            System.out.println("InvalidJson");
        } catch (InvalidDataException exc) {
            System.out.println("InvalidDataException");
        }
    }
}

class Book{
    private int id;
    private String title;

    private void print(){
        System.out.println("id: " + id + "\n" + "title: " + title);
    }
}

class User{
    @Positive
    private int id;

    @Length(min = 3, max = 9)
    private String firstName;

    @Length(max = 15)
    private String lastName;

    private String email;

    private void print(){
        System.out.println("id: " + id + "\n" + "firstName: " + firstName + "\n" + "lastName: " + lastName + "\n" + "email: " + email);
    }
}
