package itis.sem2.hw9;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter URL: ");
        String surl = scanner.nextLine().trim();
        URL url = new URL(surl);
        InputStream inputStream = url.openStream();
        Files.copy(inputStream, new File("./DownloadsTask2/page.html").toPath());
        Document doc = Jsoup.parse(url, 500000);

        Elements collectionImg = doc.select("img[src]");
        Elements collectionLink = doc.select("link[href$=.css]");
        Elements collectionScript = doc.select("script[src$=.js]");

        download(collectionLink, "href");
        download(collectionScript, "src");
        download(collectionImg, "src");
    }

    public static void download(Elements elements, String key){
        for (int i = 0; i < elements.size(); i++){
            try{
                String element = elements.get(i).absUrl(key);
                String way = "./DownloadsTask2/";
                String[] folders = element.split("://")[1].split("/");
                /*if (elements.get(i).absUrl(key).equals(elements.get(i))) {
                    for (int j = 0; j < folders.length - 1; j++) {
                        way = way + folders[j] + "/";
                    }
                } else {*/
                    for (int j = 1; j < folders.length - 1; j++) {
                        way = way + folders[j] + "/";
                    }
                /*}*/
                File directory = new File(way);
                directory.mkdirs();
                Files.copy((new URL(element)).openStream(), new File(way + folders[folders.length-1]).toPath());
            } catch (MalformedURLException urlexc){
                System.out.println("Err: wrong URL!");;
            } catch (FileAlreadyExistsException e) {
                System.out.println("Err: file already exists");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}