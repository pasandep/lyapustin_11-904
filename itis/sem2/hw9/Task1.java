package itis.sem2.hw9;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Task1 {
    public static <Url> void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter URL: ");
        Document doc = Jsoup.parse(new URL(scanner.nextLine()), 500000);

        System.out.print("Enter format: ");
        String format = scanner.nextLine();

        Elements collections1 = doc.select("img[src$=" + format + "]");
        Elements collections2 = doc.select("a[href$=" + format + "]");
        for (int i = 0; i < collections1.size(); i++){
            try {
                String url = collections1.get(i).absUrl("src");
                URL srcurl = new URL(url);
                InputStream inputStream = srcurl.openStream();
                Files.copy(inputStream, new File("./DownloadsTask1/" + "src" + i + format).toPath());
            } catch (IOException exc) {
                System.out.println("Error: " + collections1.get(i).toString());
            }
        }
        for (int i = 0; i < collections2.size(); i++){
            try {
                String url = collections2.get(i).absUrl("href");
                URL hrefurl = new URL(url);
                InputStream inputStream = hrefurl.openStream();
                Files.copy(inputStream, new File("DownloadsTask1/" + "href" + i + format).toPath());
            } catch (IOException exc) {
                System.out.println("Error: " + collections1.get(i).toString());
            }
        }
    }
}
