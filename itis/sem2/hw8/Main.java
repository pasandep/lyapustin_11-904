package itis.sem2.hw8;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws IOException {
        File file = new File("itis/sem2/hw8/input");
        Scanner scanner = new Scanner(file);
        ArrayList<String> array = new ArrayList<>();
        while (scanner.hasNextLine()){
            array.add(scanner.nextLine());
        }
        Pattern threeWords = Pattern.compile("(\\S+\\s+){2}\\S(\\s+\\S)*");

        array.stream()
                .filter(x -> threeWords.matcher(x).find())
                .distinct()
                .forEach(System.out::println);

        System.out.println("--------------------");

        System.out.println(array.stream()
                .mapToInt(x -> x.length())
                .min()
                .orElse(-1));

        System.out.println("--------------------");

        System.out.println(array.stream()
                .mapToInt(x -> x.split("\\s+").length)
                .average()
                .orElse(-1));
    }
}