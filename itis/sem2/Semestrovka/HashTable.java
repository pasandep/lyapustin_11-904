package itis.sem2.Semestrovka;

import itis.exam.Array;

import java.util.ArrayList;
import java.util.Scanner;

public class HashTable<K, V> {

    private class Element<K, V>{
        private V value;
        private K key;
        private Element<K, V> next;

        Element(K key, V value){
            this.value = value;
            this.key = key;
        }
    }

    Element<K, V>[] elements = new Element[100];

    // добавляет элемент в таблицу
    public boolean add(K key, V value){
        Element<K, V> element = new Element<>(key, value);
        int index = hashFunction(key);
        if(elements[index] == null){
            elements[index] = element;
            return true;
        } else {
            Element<K, V> helper = elements[index];
            while(helper.next != null){
                helper = helper.next;
            }
            helper.next = element;
            return true;
        }
    }

    // хеш функция вовращает последние две цифры от хеш кода
    private int hashFunction(K key){
        return Math.abs(key.hashCode() % 100);
    }

    // Вовзвращает значение по ключу. Если ключу соответсвует несколько значений предоставляет пользователю выбор
    public V getByKey(K key){
        int index = hashFunction(key);
        if (elements[index] != null){
            ArrayList<Element <K, V>> returner = new ArrayList<>();
            Element<K, V> helper = elements[index];
            while (helper.next != null){
                if (helper.key.equals(key)){
                    returner.add(helper);
                }
                helper = helper.next;
            }
            if (helper.key.equals(key)){
                returner.add(helper);
            }
            if (returner.size() > 0){
                if (returner.size() == 1) {
                    return returner.get(0).value;
                } else {
                    System.out.println("Choose one (for key " + key +"):");
                    for (int i = 0; i < returner.size(); i++){
                        System.out.println(i + ": " + returner.get(i).value);
                    }
                    Scanner scanner = new Scanner(System.in);
                    int n = scanner.nextInt();
                    if (n < returner.size() && n >= 0) {
                        return returner.get(n).value;
                    } else {
                        System.out.println("Wrong index!");
                        return null;
                    }
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    // удаляет все элементы соответствующие данному ключу
    public boolean removeAllByKey(K key){
        int index = hashFunction(key);
        if (elements[index] == null){
            return false;
        }
        Element<K, V> helper = elements[index];
        if (elements[index].next != null) {
            while (helper.next.next != null) {
                if (helper.next.key.equals(key)) {
                    helper.next = helper.next.next;
                } else {
                    helper = helper.next;
                }
            }
            if (helper.next.key.equals(key)) {
                if (helper.next != null) {
                    helper.next = helper.next.next;
                } else {
                    helper.next = null;
                }
            }
        }
        if (elements[index].key.equals(key)){
            if (elements[index].next != null){
                elements[index] = elements[index].next;
            } else {
                elements[index] = null;
            }
        }
        return true;
    }

    // удаляет первый элемент соответствующий заданному ключу
    public boolean removeFirst(K key){
        int index = hashFunction(key);
        if (elements[index] == null){
            return false;
        }
        if (elements[index].key.equals(key)){
            if (elements[index].next == null){
                elements[index] = null;
                return true;
            } else {
                elements[index] = elements[index].next;
                return true;
            }
        }
        Element<K, V> helper = elements[index];
        while (helper.next != null && !helper.next.key.equals(key)){
            helper = helper.next;
        }
        helper.next = helper.next.next;
        return true;
    }

    // проверяет наличие элемента соответвующего заданному ключу
    public boolean containsKey(K key) {
        int index = hashFunction(key);
        Element<K, V> helper = elements[index];
        if (elements[index] != null) {
            while (helper.next != null) {
                if (helper.key.equals(key)) {
                    return true;
                }
                helper = helper.next;
            }
            if (helper.key.equals(key)) {
                return true;
            }
            return false;
        } return false;
    }
}