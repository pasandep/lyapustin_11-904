package itis.sem2.Semestrovka;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) {
        try {
            File file = new File("itis\\sem2\\Semestrovka\\input");
            HashTable<String, String> hashTable = new HashTable();
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()){
                StringTokenizer stringTokenizer = new StringTokenizer(scanner.nextLine(), "-");
                hashTable.add(stringTokenizer.nextToken().trim(), stringTokenizer.nextToken().trim());
            }

            System.out.println("База данных - " + hashTable.getByKey("База данных"));

            System.out.println(hashTable.containsKey("Индукция"));

            var string = hashTable.getByKey("Индукция");
            System.out.println("Индукция - " + string);

            hashTable.removeFirst("Индукция");
            string = hashTable.getByKey("Индукция");
            System.out.println("Индукция - " + string);

            hashTable.removeAllByKey("Индукция");
            System.out.println(hashTable.getByKey("Индукция"));

            System.out.println(hashTable.containsKey("Индукция"));

        } catch (FileNotFoundException fnfe){
            System.out.println("File not founded!");
        }
    }
}
