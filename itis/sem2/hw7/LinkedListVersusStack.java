package itis.sem2.hw7;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

public class LinkedListVersusStack {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        LinkedList<Integer> forAddAll = new LinkedList<>();
        for (int i = 0; i < 1000; i++) {
            forAddAll.push(random.nextInt());
        }
        for (int j = 100000; j <= 1000000; j+=100000 ) {

            LinkedList<Integer> linkedList = new LinkedList<>();

            for (int i = 0; i < j; i++) {
                linkedList.push(random.nextInt());
            }

            long linkedListPushStart = System.nanoTime();
            for (int i = 0; i < 10; i++){
                linkedList.push(random.nextInt());
            }
            long linkedListPushEnd = System.nanoTime();
            long linkedListPushTime = (linkedListPushEnd - linkedListPushStart)/10;

            long linkedListPeekStart = System.nanoTime();
            for (int i = 0; i < 10; i++){
                linkedList.peek();
            }
            long linkedListPeekEnd = System.nanoTime();
            long linkedListPeekTime = (linkedListPeekEnd - linkedListPeekStart)/10;

            long linkedListPopStart = System.nanoTime();
            for (int i = 0; i < 10; i++){
                linkedList.pop();
            }
            long linkedListPopEnd = System.nanoTime();
            long linkedListPopTime = (linkedListPopEnd - linkedListPopStart)/10;

            long linkedListAddAllStart = System.nanoTime();
            for (int i = 0; i < 10; i++) {
                linkedList.addAll(forAddAll);
            }
            long linkedListAddAllEnd = System.nanoTime();
            long linkedListAddAllTime = (linkedListAddAllEnd - linkedListAddAllStart)/10;

            long linkedListProhodStart = System.nanoTime();
            for (int i = 0; i < 10; i++) {
                for (Integer element : linkedList) {
                    element = element + 2;
                }
            }
            long linkedListProhodEnd = System.nanoTime();
            long linkedListProhodTime = (linkedListProhodEnd - linkedListProhodStart)/10;
            //---------------------------------------------------------

            Stack<Integer> stack = new Stack<>();

            for (int i = 0; i < j; i++) {
                stack.push(random.nextInt());
            }

            long stackPushStart = System.nanoTime();
            for (int i = 0; i < 10; i++){
                stack.push(random.nextInt());
            }
            long stackPushEnd = System.nanoTime();
            long stackPushTime = (stackPushEnd - stackPushStart)/10;

            long stackPeekStart = System.nanoTime();
            for (int i = 0; i < 10; i++){
                stack.peek();
            }
            long stackPeekEnd = System.nanoTime();
            long stackPeekTime = (stackPeekEnd - stackPeekStart)/10;

            long stackPopStart = System.nanoTime();
            for (int i = 0; i < 10; i++){
                stack.pop();
            }
            long stackPopEnd = System.nanoTime();
            long stackPopTime = (stackPopEnd - stackPopStart)/10;

            long stackAddAllStart = System.nanoTime();
            for (int i = 0; i < 10; i++) {
                stack.addAll(forAddAll);
            }
            long stackAddAllEnd = System.nanoTime();
            long stackAddAllTime = (stackAddAllEnd - stackAddAllStart)/10;

            long stackProhodStart = System.nanoTime();
            for (int i = 0; i < 10; i++) {
                for (Integer element : stack) {
                    element = element + 2;
                }
            }
            long stackProhodEnd = System.nanoTime();
            long stackProhodTime = (stackProhodEnd - stackProhodStart)/10;

            //---------------------------------------------------------------------------

            System.out.println("For " + j + ": ");
            System.out.println("Push: " + "\n"
                    + "LinkedList: " + linkedListPushTime + "\n"
                    + "Stack: " + stackPushTime + "\n"
                    + "Different: " + Math.abs(linkedListPushTime - stackPushTime));
            System.out.println();

            System.out.println("Peek: " + "\n"
                    + "LinkedList: " + (linkedListPeekTime) + "\n"
                    + "Stack: " + stackPeekTime + "\n"
                    + "Different: " + Math.abs(linkedListPeekTime - stackPeekTime));
            System.out.println();

            System.out.println("Pop: " + "\n"
                    + "LinkedList: " + linkedListPopTime + "\n"
                    + "Stack: " + stackPopTime + "\n"
                    + "Different: " + Math.abs(linkedListPopTime - stackPopTime));
            System.out.println();

            System.out.println("AddAll: " + "\n"
                    + "LinkedList: " + linkedListAddAllTime + "\n"
                    + "Stack: " + stackAddAllTime + "\n"
                    + "Different: " + Math.abs(linkedListAddAllTime - stackAddAllTime));
            System.out.println();

            System.out.println("ForEach: " + "\n"
                    + "LinkedList: " + linkedListProhodTime + "\n"
                    + "Stack: " + stackProhodTime + "\n"
                    + "Different: " + Math.abs(linkedListProhodTime + stackProhodTime));
            System.out.println("---------------------------------------------------------------------");
        }
    }
}
