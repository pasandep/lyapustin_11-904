package itis.Test1_Try2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int ku = 0; // Переменная необходимя для проверки чисел на убивание
        int saver;
        int k = 0; // Показывает количество точек максимума
        int kv = 0; // Необходима для подсчёта возрастаний
        while (n != 0) {
            saver = n;
            n = scanner.nextInt();
            if (n != 0) {
                if (n > saver) {
                    kv++;
                }
                if ((n < saver) && (ku == 0) && (kv > 0)) {
                    ku++;
                    k++;
                } else if (n > saver) {
                    ku = 0;
                }
            }
        }
        if (k == 1){
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}
