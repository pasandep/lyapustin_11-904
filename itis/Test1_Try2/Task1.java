package itis.Test1_Try2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите s - количество минут: ");
        int s = scanner.nextInt();
        while (s > 1439) {
            s -= 1440;
        }
        if ((s >= 0) && (s <= 299)){
            System.out.println("Night");
        } else if ((s >= 300) && (s <= 719)){
            System.out.println("Morning");
        } else if ((s >= 720) && (s <= 1019)){
            System.out.println("Day");
        } else if ((s >= 1020) && (s <= 1439)){
            System.out.println("Evening");
        } else {
            System.out.println("Err!");
        }
    }
}


