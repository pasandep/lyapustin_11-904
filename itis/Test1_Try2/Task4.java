package itis.Test1_Try2;

public class Task4 {
    public static void main(String[] args) {
        double k = 0;
        double n = 2;
        double a = 0.25;
        int z = 16;
        double b = (((n * n) + n - 1)/z);
        double sum = a;
        while((k = (a - b) > 0 ? a-b:b-a) > 0.00001) {
            sum += b;
            a = b;
            n++;
            z *= 4;
            b = ((n * n + n - 1) / z);
        }
        System.out.println("sum = " + sum);
    }
}