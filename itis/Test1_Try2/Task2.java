package itis.Test1_Try2;
import java.util.Scanner;

public class Task2{

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter n: ");
        int n = sc.nextInt();

        if ((n % 2 != 0) && (n > 0)){
            for (int i = 1; i <= (n+1)/2; i++){
                for (int j = 1; j <= (n+1)/2-i; j++){
                    System.out.print(" ");
                }
                for (int j = 1; j < 2*i; j++){
                    System.out.print("*");
                }
                for (int j = 1; j <= (n+1)/2-i; j++){
                    System.out.print(" ");
                }
                System.out.println();
            }

            for (int i = 1; i <= n-((n+1)/2); i++){
                for (int j = 1; j <= i; j++){
                    System.out.print(" ");
                }
                for (int j = 1; j <= n-2*i; j++){
                    System.out.print("*");
                }
                for (int j = 1; j <= i; j++){
                    System.out.print(" ");
                }
                System.out.println();
            }
        } else {
            System.out.print("Err!");
        }
    }
}
