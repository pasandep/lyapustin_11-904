package itis.homework10;

public class ArrayUtil {
    public static int[] DeleteNumber(final int[] a, int k){
        int[] b = new int[a.length-1];
        for (int i = 0; i < k; i++){
            b[i] = a[i];
        }
        for (int i = k+1; i < a.length; i++){
            b[i-1] = a[i];
        }
        return b;
    }

    public static int[] DeleteNumbers(final int[] a, int k, boolean flag){
        int[] c = new int[a.length];
        int j = -1;
        int kf = 0; // счётчик необходимый для false
        int k0 = 0; // счётчик нулей в конце массива
        if (flag){
            for (int i = 0; i < a.length; i++) {
                j++;
                if (a[i] != k) {
                    c[j] = a[i];
                } else {
                    k0++;
                    j--;
                }
            }
        } else {
            for (int i = 0; i < a.length; i++){
                j++;
                if (a[i] == k){
                    if(kf != 0){
                        c[j] = a[i];
                    } else {
                        k0++;
                        kf++;
                        j--;
                    }
                } else {
                    c[j] = a[i];
                }
            }
        }
        int[] b = new int[c.length - k0];
        for (int i = 0; i < b.length; i++){
            b[i] = c[i];
        }
        return(b);
    }
}

