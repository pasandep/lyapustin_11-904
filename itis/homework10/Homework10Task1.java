package itis.homework10;

import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class Homework10Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();

        System.out.print("Enter n: ");
        int n = sc.nextInt();
        if (n > 0) {

            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = r.nextInt(n);
                System.out.print(a[i] + " ");
            }

            System.out.println();
            System.out.print("Enter k: ");
            int k = sc.nextInt();
            if ((k >= 0) && (k < n)) {

                int[] b = ArrayUtil.DeleteNumber(a, k);
                System.out.print("Answer: ");
                for (int i = 0; i < b.length; i++) {
                    System.out.print(b[i] + " ");
                }
            } else {
                System.out.println("Err! k most be an index of number!");
            }
        } else {
            System.out.println("Err! n must be positive!");
        }
    }
}
