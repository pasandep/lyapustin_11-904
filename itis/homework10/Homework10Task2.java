package itis.homework10;

import java.util.Random;
import java.util.Scanner;

public class Homework10Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();

        System.out.print("Enter n: ");
        int n = sc.nextInt();
        if (n > 0) {
            int[] a = new int[n];
            for (int i = 0; i < a.length; i++) {
                a[i] = r.nextInt(n);
                System.out.print(a[i] + " ");
            }

            System.out.println();
            System.out.print("Enter number that you want to delete: ");
            int k = sc.nextInt();


            System.out.print("Enter true or false: ");
            boolean flag = sc.nextBoolean();

            int[] b = ArrayUtil.DeleteNumbers(a, k, flag);

            System.out.print("Answer: ");
            for (int i = 0; i < b.length; i++) {
                System.out.print(b[i] + " ");
            }
        } else {
            System.out.print("Err! N must be positive!");
        }
    }
}
