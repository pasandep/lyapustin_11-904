package itis.TEST2;


import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        Pattern pattern = Pattern.compile("[1-9][0-9]*[.,]?[0-9]+");
        Matcher matcher = pattern.matcher(s);
        StringBuffer stringBuffer = new StringBuffer();
        double sum = 0;
        double n = 0;
        while (matcher.find()){
            System.out.print(s.substring(matcher.start(), matcher.end()).replace(",", "."));
            sum = sum + Double.valueOf(s.substring(matcher.start(), matcher.end()).replace(",", "."));
            if (s.substring(matcher.start(), matcher.end()) != null){
                System.out.print(" + ");
            }
        }
        System.out.println("= " + sum);
    }
}
