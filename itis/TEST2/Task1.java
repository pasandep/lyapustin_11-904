package itis.TEST2;

import java.util.Random;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        System.out.print("Enter n: ");
        int n = sc.nextInt();
        if (n > 0){
            System.out.print("Enter m: ");
            int m = sc.nextInt();
            if (m > 0){
                int[][] a = new int[n][m+1];
                for (int i = 0; i < n; i++){
                    for (int j = 0; j < m; j++){
                        a[i][j] = r.nextInt(11);
                        a[i][m] = a[i][m] + a[i][j];
                    }
                }
                for (int i = 0; i < n; i++){
                    for (int j = 0; j < m; j++){
                        System.out.print(a[i][j] + "  ");
                    }
                    System.out.println();
                }
                int[] c = new int[m];
                for (int i = 0; i < n-1; i++){
                    for (int j = 0; j < n-1; j++){
                        if (a[j][m] > a[j+1][m]){
                            c = a[j];
                            a[j] = a[j + 1];
                            a[j + 1] = c;
                        }
                    }
                }
                System.out.println();
                double sr;
                for (int i = 0; i < n; i++){
                    for (int j = 0; j < m; j++){
                        System.out.print(a[i][j] + "  ");
                    }
                    sr = (double)a[i][m]/(m);
                    System.out.println(" | sr = " + sr);
                }
            } else {
                System.out.println("Err: m must be positive!");
            }
        } else {
            System.out.println("Err: n must be positive!");
        }
    }
}
