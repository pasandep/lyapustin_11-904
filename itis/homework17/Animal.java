package itis.homework17;

import java.util.Random;

public class Animal {

    protected int energy;

    protected Random random = new Random();

    protected  Animal(){
        this.energy += 10;
    }

    public void sleep(){
        System.out.println("Sleeping!");
        this.energy += 5;
        System.out.println("Energy: " + energy );
        System.out.println();
    }

    public void run(){
        if (energy > 4) {
            System.out.println("Running!");
            this.energy -= 5;
        } else {
            System.out.println("Not enough energy! Sleep or eat something!");
        }
        System.out.println("Energy: " + energy );
        System.out.println();
    }

    public void eat(String food){
        System.out.println("Eating " + food);
        this.energy += random.nextInt(2) + 2;
        System.out.println("Energy: " + energy );
        System.out.println();
    }

    public int getEnergy(){
        return energy;
    }
}