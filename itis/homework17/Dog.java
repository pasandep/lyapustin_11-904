package itis.homework17;

import java.util.Objects;

public class Dog extends Animal implements Pet {

    private int countofwands = 0;
    private String nickname;



    protected Dog(String nickname) {
        super();
        this.nickname = nickname;
    }

    @Override
    public void voice() {
        System.out.println("Hell... khm,GAV!");
        System.out.println();
    }

    public void bringWand(){
        if (energy > 2){
            int chance = random.nextInt(101);
            if (chance <= 55) {
                System.out.println("Good job! Wand is here!");
                countofwands++;
            } else {
                System.out.println("Nice try, but the wand got lost somewhere along the way =(");
            }
            energy -= 3;
            System.out.println("Energy: " + energy);
        } else {
            System.out.println("Not enough energy! Sleep or eat something!");
        }
        System.out.println();
    }

    public int getCountofwands() {
        return countofwands;
    }

    public void askForAffection(){
        int chance = random.nextInt(101);
        if (chance <= 30){
            System.out.println("Someone from your human friends noticed you and stroked you!");
        } else {
            System.out.println("Now you're out of luck, friend, all the people are busy =(");
        }
        System.out.println();
    }

    public String toString() {
        return "Dog{" + "countofwands = " + countofwands + ", nickname = '" + nickname +  "'}";
    }

    public boolean equals(Dog o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return (countofwands == o.countofwands && nickname.equals(o.nickname));
    }

    public int hashCode() {
        return Objects.hash(countofwands, nickname);
    }
}
