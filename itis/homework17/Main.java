package itis.homework17;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter nickname of dog1: ");
        Dog dog = new Dog(sc.nextLine());
        System.out.print("Enter nickname of dog2: ");
        Dog dog2 = new Dog(sc.nextLine());
        if (dog.equals(dog2)){
            System.out.println("dog1 equals dog2");
            System.out.println("dog1 hashcode: " + dog.hashCode());
            System.out.println("dog2 hashcode: " + dog2.hashCode());
            System.out.println();
        } else {
            System.out.println("dog1 is not equals dog2");
            System.out.println("dog1 hashcode: " + dog.hashCode());
            System.out.println("dog2 hashcode: " + dog2.hashCode());
            System.out.println();
        }
        System.out.println("Let's play with dog1!");
        System.out.println();
        dog.eat("meat.");
        dog.run();
        dog.run();
        dog.sleep();
        dog.run();
        dog.eat("fruits");
        System.out.println("Number of sticks brought: " + dog.getCountofwands());
        System.out.println();
        dog.askForAffection();
        dog.askForAffection();
        dog.eat("fruits");
        dog.eat("meal");
        dog.bringWand();
        dog.bringWand();
        dog.bringWand();
        System.out.println(dog.toString());
        System.out.println(dog2.toString());
        System.out.println();
        if (dog.equals(dog2)){
            System.out.println("dog1 equals dog2");
            System.out.println("dog1 hashcode: " + dog.hashCode());
            System.out.println("dog2 hashcode: " + dog2.hashCode());
            System.out.println();
        } else {
            System.out.println("dog1 is not equals dog2");
            System.out.println("dog1 hashcode: " + dog.hashCode());
            System.out.println("dog2 hashcode: " + dog2.hashCode());
            System.out.println();
        }
    }
}