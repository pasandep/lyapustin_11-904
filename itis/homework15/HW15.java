package itis.homework15;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HW15 {
    public static void main(String[] args) {
        try {
            String str = "";
            int k = 0;
            Pattern pattern1 = Pattern.compile("\\b[^1][0]+\\b");
            Pattern pattern2 = Pattern.compile("\\b[^0][1]+\\b");
            Pattern pattern3 = Pattern.compile("\\b1?(01)+0?\\b");
            BufferedReader bufferedReader = new BufferedReader(new FileReader("./itis/homework15/input.txt"));
            System.out.println("Correct lines: ");
            while((str = bufferedReader.readLine()) != null) {
                k++;
                Matcher matcher1 = pattern1.matcher(str);
                Matcher matcher2 = pattern2.matcher(str);
                Matcher matcher3 = pattern3.matcher(str);
                if (matcher1.find()){
                    System.out.println(k + ": Match with 1st pattern.");
                }
                if (matcher2.find()){
                    System.out.println(k + ": Match with 2nd pattern.");
                }
                if (matcher3.find()){
                    System.out.println(k + ": Match with 3rd pattern.");
                }
            }
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
