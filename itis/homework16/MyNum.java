package itis.homework16;

public class MyNum {

    private int value;
    private int sum;

    public MyNum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    private int privateSumOfDigit() {
        int sum = 0;
        int method_value = value;
        method_value = method_value < 0? -method_value:method_value;
        while (method_value != 0) {
            sum = sum + method_value % 10;
            method_value = method_value / 10;
        }
        return sum;
    }

    public void printSumOfDigit(){
        System.out.println("Sum of digit = " + privateSumOfDigit());
    }

    private int privateSumOfDigitByRecursion(){
       // int value_save = value;
        value = value < 0? -value:value;
        sum+= value % 10;
        if (value > 9){
            value/=10;
            sum = privateSumOfDigitByRecursion();
        }
     //   value = value_save;
        return sum;
    }

    public void printSumOfDigitByRecursion(){
        System.out.println("Sum of digit = " + privateSumOfDigitByRecursion());
    }

}