package itis.homework16;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter number: ");
        MyNum myNum = new MyNum(sc.nextInt());
        System.out.println("-----------------------------------");
        System.out.println("Value of number: " + myNum.getValue()); // Проверка на получение value извне.
        System.out.println("-----------------------------------");
        myNum.printSumOfDigit(); //  Проверка вывода суммы цифр.
        System.out.println("-----------------------------------");
        myNum.printSumOfDigitByRecursion(); // Проврерка вывода суммы цифр через рекурсию.
    }
}
