package itis.homework21;

import java.util.Random;
import java.util.Scanner;

public class Game {
    private Player player1;
    private Player player2;

    Scanner scanner = new Scanner(System.in);

    public void start(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
        Random random = new Random();
        int n = random.nextInt(100) + 1;
        if (n > 50){
            System.out.println("Coin decided that the " + player2.getName() + " will move first!");
            System.out.print(player2.getName() + ", choose power of hit(1-9): ");
            player1.setHp(player1.getHp() - player2.hit(scanner.nextInt()));
        } else {
            System.out.println("Coin decided that the " + player1.getName() + " will move first!");
        }
        while (player1.getHp() > 0 && player2.getHp() > 0){
            System.out.print(player1.getName() + ", choose power of hit(1-9): ");
            player2.setHp(player2.getHp() - player1.hit(scanner.nextInt()));
            if (player2.getHp() > 0) {
                System.out.print(player2.getName() + ", choose power of hit(1-9): ");
                player1.setHp(player1.getHp() - player2.hit(scanner.nextInt()));
            }
        }
        if (player1.getHp() > player2.getHp()){
            System.out.println("!!! " + player1.getName() + " win !!!");
        } else {
            System.out.println("!!! " + player1.getName() + " win !!!");
        }
        System.out.println("GAME OVER!");
    }
}
