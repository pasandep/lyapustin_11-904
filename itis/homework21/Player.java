package itis.homework21;

import java.util.Random;

public class Player {
    private int hp;
    private String name;

    public Player(String name) {
        this.name = name;
        hp = 100;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
        System.out.println("Health point of " + getName() + " is " + getHp() + "!");
        System.out.println();
    }

    Random radnom = new Random();

    public int hit(int HitPower) {
        if (HitPower > 0 && HitPower < 10) {
            int n = radnom.nextInt(101);
            if (n < 100 - HitPower * 10) {
                    System.out.println("Hit!");
                    return HitPower;
            } else {
                System.out.println("Miss!");
                return 0;
            }
        } else {
            System.out.println("You indicated the wrong power, now you are punished and miss your turn!");
            return 0;
        }
    }
}

