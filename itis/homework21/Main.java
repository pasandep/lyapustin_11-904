package itis.homework21;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner =  new Scanner(System.in);
        System.out.print("Enter nickname of first player: ");
        Player player1 = new Player(scanner.nextLine());
        System.out.print("Enter nickname of second player: ");
        Player player2 = new Player(scanner.nextLine());
        Game game = new Game();
        game.start(player1, player2);
    }
}
