package itis.homework7;
import java.util.Scanner;

public class Sum{
	
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter n: ");
		int n = sc.nextInt();
		int sum = 1;   // Любое число, в том числе и простое делится на единицу без остатка, поэтому сумму берём сразу с единицей
		
		for (int i = 2; i <= n; i++){   // Проверяю для n, а не для n/2 чтобы в случае введёного простого числа цикл дошёл до него самого.
			if (n % i == 0){
				
				while (n % i == 0){      // Если число делится на n, то не нужно проверять делится ли оно на 2*n, 3*n
					n /= i;
					
				}
				sum += i;
			}
		}	
		System.out.print("Answer: " + sum);
	}
}