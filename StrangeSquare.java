package itis.homework7;
import java.util.Scanner;

public class StrangeSquare{
	
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter n: ");
		int n = sc.nextInt();
		
		if (n % 2 == 0) {
			for (int i = 1; i <= n/2; i++){
				
				for (int j = 1; j <= n/2; j++){
					if (i >= j){                  //Если строка больше или равна столбцу, то выводим значние строки, иначе столбца
						System.out.print(i);
					} else {
						System.out.print(j);
					}
				}
				
				for (int j = n/2+1; j <= n; j++){
					System.out.print(j+i);
				}
				System.out.println();
				
			}
			
			for (int i = n/2+1; i <= n; i++){
				
				for (int j = 1; j <= n/2; j++){
					System.out.print(j+i);
				}
				
				for (int j = n/2+1; j <= n; j++){
					if (i <= j){                   //Если строка больше или равна столбцу, то выводим значние строки, иначе столбца  
						System.out.print(i);
					} else {
						System.out.print(j);
					}
				}
				System.out.println();
			}
		} else {
			System.out.print("Err!");
		}
	}
}