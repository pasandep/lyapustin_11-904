import java.util.Scanner;

public class HomeworkTask1{
	
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
		System.out.print("Enter n:");
		int n = sc.nextInt();
		
		if (n > 0) {
			
			System.out.println("Enter " + n +  " numbers:");
			int max = sc.nextInt();
			int min = max;
			
			for (int i = 2; i <= n; i++) {
				int a = sc.nextInt();
				if (a > max) {
					max = a;
				} else if(a < min) {
					min = a;
				}
			} System.out.print("Answer: " + (max - min));
		} else {
			System.out.print("N must be positive!");
		}
	}	
}