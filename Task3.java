package itis.test1;
import java.util.Scanner;

public class Task3{
	
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter n: ");
		int n = sc.nextInt();
		int sum = 0;  
		
		if (n > 0){
			for (int i = n-1; i > 0; i--){
				if (n % i == 0){
					sum += i;
				}
			}
			if (n == sum){
				System.out.print("Yes!");
			} else {
				System.out.print("No!");		
			}
		} else {
			System.out.print("Err!");
		}
	}
}
