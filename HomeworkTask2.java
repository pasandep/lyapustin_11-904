import java.util.Scanner;

public class HomeworkTask2{
	
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
		System.out.print("Enter n:");
		int n = sc.nextInt();
		
		if (n > 0) {
			
			System.out.println("Enter " + n +  " numbers:");
			int max = sc.nextInt();
			int min1 = max;
			int min2 = max;
			
			for (int i = 2; i <= n; i++) {
				int a = sc.nextInt();
				if (a > max) {
					max = a;
				} else if(a < min1) {
					if (min2 >= min1){
						min2 = min1;
					}
					min1 = a;
				}
				} System.out.println("Answer: " + (max - min2));
				  System.out.print(max + " " + min2);
			} else {
			System.out.print("N must be positive!");
		}
	}		
}

