package itis.test1;
import java.util.Scanner;

public class Task1{

    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String a = "";
		String b;
		
		for (int i = 1; i <= 3; i++){
			System.out.println("Enter x" + i + " and y" + i + " :");
			int x = sc.nextInt();
			int y = sc.nextInt();
			
			
			if ((x != 0) || (y != 0)) {
				if (x == 0) {
					b ="| x axis |";
					a = a + b;
				}
				if (y == 0) {
					b ="| y axis |";
					a = a + b;
				}
			
				if (x > 0) {
					if (y > 0) {
						b ="| I |";
						a = a + b;
					} else if (y != 0) {
						b ="| IV |";
						a = a + b;
					}
				} else if (x != 0) {
					if (y > 0) {
						b ="| II |";
						a = a + b;
					} else if (y != 0){
						b ="| III |";
						a = a + b;
					}
				}	
			} else if ((x == 0) && (y == 0)){
				b ="| Origin |";
				a = a + b;
			}
			b = "";
		}
		System.out.print(a);	
	}	
}
