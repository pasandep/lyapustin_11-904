package itis.homework7;
import java.util.Scanner;

public class SandClocks{
	
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter n: ");
		int n = sc.nextInt();
		
		if (n % 2 != 0) {
			for (int i = 0; i < (n+1)/2; i++){     // Обработка до середины часов включительно
				
				for (int j = 0; j < i; j++ ){
					System.out.print(" ");
				}
				
				for (int j = 1; j <= n-2*i; j++){
					System.out.print("*");
				}
				System.out.println();
			}
			for (int i = ((n+1)/2)+1; i <= n; i++){  // Обработкы оставшей части часов
				for (int j = 1; j <= n-i; j++ ){
					System.out.print(" ");
				}
				
				for (int j = i; j > n-i; j--){
					System.out.print("*");
				}
				System.out.println();
			}
		} else {
			System.out.print("Err!");
		}
	}
}